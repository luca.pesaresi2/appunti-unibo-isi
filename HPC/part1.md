### Part 1

- Power = $`C \times V^2 \times f`$
    - C: capacitance (ability of a circuit to store energy)
    - V: voltage
    - f: frequency at which the processor operates
- Limits
    - Power Wall: clock frequency cannot be increased without exceeding air cooling
    - Memory Wall: access to data is a limiting factor
    - ILP Wall: all the existing instruction-level parallelism (ILP) is already being used
- Parallel programming in brief: writing efficient parallel code requires that the programmer understands, and makes explicit use of, the underlying hardware
    - Decompose the problem in sub-problems
    - Distribute sub-problems to the available execution units
    - Solve sub-problems independently
        - Cooperate to solve sub-problems
- Example (Sum-Reduction, compute the sum of the content of an array A of length n)
    - Sequential algorithm
        ```c
        float seq_sum(float* A, int n) {
            int i;
            float sum = 0.0;
            for (i=0; i<n; i++) {
                sum += A[i];
            }
            return sum;
        }
        ```
    - Parallel algorithm (P = n_proc, n = tot_values)
        - Version 1
            ```c
            my_start = n * my_id / P;
            my_end = n * (my_id + 1) / P;
            psum[0..P-1] = 0.0;
            for (my_i=my_start; my_i<my_end; my_i++) {
                my_x = get_value(my_i);
                psum[my_id] += my_x;
            }
            barrier();
            if ( 0 == my_id ) { 
                sum = 0.0;
                for (my_i=0; my_i<P; my_i++) 
                    sum += psum[my_i];
            }
            ```
        - Version 2
            - Each processor computes a local sum
            - Each processor sends the local sum to processor 0 (the master)
            ```c
            ...
            my_sum = 0.0;
            my_start = ..., my_end = ...;
            for ( i = my_start; i < my_end; i++ ) {
                my_sum += get_value(i);
            }
            if ( 0 == my_id ) {
                for ( i=1; i<P; i++ ) {
                    tmp = receive from proc i;
                    my_sum += tmp;
                }
                printf(“The sum is %f\n”, my_sum);
            } 
            else {
                send my_sum to thread 0;
            }
            ```

            ![alt text](./res/v5-botneck.png "Version 2 bottleneck")

            ![alt text](./res/par-red.png "Possible parallel reduction")

- Task parallelism vs Data parallelism
    - Task Parallelism: distribute (possibly different) tasks to processors
    - Data Parallelism: distribute data to processors, each processor executes the same task on different data