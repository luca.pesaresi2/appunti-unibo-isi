/****************************************************************************
 *
 * simd-vsum-auto : Vector sum; this program is used to test compiler
 * auto-vectorization.
 *
 * Last updated in 2018 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all 
 * copyright and related and neighboring rights to this software to the 
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see 
 * <http://creativecommons.org/publicdomain/zero/1.0/>. 
 *
 * --------------------------------------------------------------------------
 *
 * Compile with:
 *
 * gcc -march=native -O2 -ftree-vectorize -ftree-vectorizer-verbose=1 simd-vsum-auto.c -o simd-vsum-auto
 *
 * and observe that the loop in vec_sum is not vectorized. To see why,
 * compile with:
 *
 * gcc -march=native -O2 -ftree-vectorize -ftree-vectorizer-verbose=2 simd-vsum-auto.c -o simd-vsum-auto
 *
 * To forse vectorization anyway, use -funsafe-math-optimizations
 *
 * gcc -funsafe-math-optimizations -march=native -O2 -ftree-vectorize -ftree-vectorizer-verbose=2 simd-vsum-auto.c -o simd-vsum-auto
 *
 * To see the assembly output:
 *
 * gcc -funsafe-math-optimizations -march=native -c -Wa,-adhln -g -O2 -ftree-vectorize -ftree-vectorizer-verbose=2 simd-vsum-auto.c > simd-vsum-auto.s
 *
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

float vsum(float *v, int n)
{
    float s = 0.0;
    int i;
    for (i=0; i<n; i++) {
        s += v[i];
    }
    return s;
}

void fill(float *v, int n)
{
    int i;
    for (i=0; i<n; i++) {
        v[i] = i%10;
    }
}

int main( int argc, char *argv[] )
{
    float *vec;
    int n = 1024;

    if ( argc > 2 ) {
        fprintf(stderr, "Usage: %s [n]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        n = atoi(argv[1]);
    }
    vec = (float*)malloc(n+sizeof(*vec));
    fill(vec, n);
    printf("sum = %f\n", vsum(vec, n));
    free(vec);
    return EXIT_SUCCESS;
}
