/****************************************************************************
 *
 * simd-vsum-intrinsics.c - Vector sum using SSE intrinsics
 *
 * Last updated in 2016 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all 
 * copyright and related and neighboring rights to this software to the 
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see 
 * <http://creativecommons.org/publicdomain/zero/1.0/>. 
 *
 * --------------------------------------------------------------------------
 *
 * Compile with:
 *
 * gcc -std=c99 -Wall -Wpedantic -mtune=native -lm simd-vsum-intrinsics.c -o simd-vsum-intrinsics
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <math.h>  /* for fabs() */

/* Return the sum of the n values stored in v,m using SIMD intrinsics.
   no particular alignment is required for v; n can be arbitrary */
float vsum_vector(float *v, int n)
{
    __m128 vv, vs;
    float ss = 0.0;
    int i;

    vs = _mm_setzero_ps(); /* vs = {0.0f, 0.0f, 0.0f, 0.0f} */
    for (i=0; i<n-4+1; i += 4) {
        vv = _mm_loadu_ps( &v[i] );     /* load four floats into vv */
        vs = _mm_add_ps(vv, vs);        /* vs = vs + vv */
    }

    /* Horizontal sum */
    ss = vs[0] + vs[1] + vs[2] + vs[3]; /* variables of type __m128 can be used as vectors */

    /* Take care of leftovers */
    for ( ; i<n; i++) {
        ss += v[i];
    }
    return ss;
}

float vsum_scalar(const float *v, int n)
{
    float s = 0.0;
    int i;
    for (i=0; i<n; i++) {
        s += v[i];
    }
    return s;
}

void fill(float *v, int n)
{
    int i;
    for (i=0; i<n; i++) {
        v[i] = i%10;
    }
}

int main( int argc, char *argv[] )
{
    float *vec;
    int n = 1024;
    float vsum_s, vsum_v;

    if ( argc > 2 ) {
        fprintf(stderr, "Usage: %s [n]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        n = atoi(argv[1]);
    }

    /* The memory does not need to be aligned, since
       we are using the load unaligned intrinsic */
    vec = (float*)malloc(n*sizeof(*vec));
    fill(vec, n);
    vsum_s = vsum_scalar(vec, n);
    vsum_v = vsum_vector(vec, n);
    printf("Scalar sum=%f, vector sum=%f\n", vsum_s, vsum_v);
    if ( fabs(vsum_s - vsum_v) > 1e-5 ) {
        fprintf(stderr, "Test FAILED\n");
    } else {
        fprintf(stderr, "Test OK\n");
    }
    free(vec);
    return EXIT_SUCCESS;
}
