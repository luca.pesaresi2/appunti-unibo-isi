### Parte 5 e 6 - Algebra relazionale

- **Cos'è e informazioni**
    * Linguaggio di manipolazione (DML) che permette di interrogare e modificare istanze di basi di dati, un altro esempio è il calcolo relazionale o l'SQL.
    * Più precisamente è un linguaggio procedurale di tipo algebrico i cui operandi sono relazioni.
    * L'SQL incorpora aspetti di calcolo relazionale e algebra relazionale.
    * L’insieme delle operazioni dell'AR non è Turing-completo.
    * La principale limitazione dell'AR è legata all'impossibilita di esprimere interrogazioni ricorsive.
- **Introduzione**
    * **Operatori di base**
        + Si applicano a una o più relazioni e producono una relazione.
        + **Tipi**
            - **Unari**
                * σ = selezione
                * π = proiezione
                * ρ = ridenominazione
            - **Binari**
                * ∪ = unione
                * − = differenza
                * \>< = join naturale
        + Altri operatori derivati possono essere definiti a partire da quelli di base.
    * **Semantica** di ogni operatore si definisce specificando:
        + Come lo schema (insieme di attributi) del risultato dipende dallo schema degli operandi.
        + Come lo stato della relazione risultato dipende dagli stati delle relazioni in ingresso.
        + Gli operatori si possono comporre, dando luogo a espressioni algebriche di complessità arbitraria.
        + Gli operandi sono o (nomi di) relazioni del DB o espressioni (ben formate).
- **Operatori di base**
    * **Selezione**
        + Permette di selezionare un sottoinsieme delle tuple di una relazione, applicando a ciascuna di esse una formula booleana F.
        + F si compone di predicati connessi da AND(∧), OR(∨) e NOT(¬).
        + Ogni predicato è del tipo A θ c  oppure A θ B, dove:
            - A ∈ X e B ∈ X sono attributi
            - c ∈ dom(A) è una costante
            - θ è un operatore di confronto, θ ∈ {=, ≠, <, >, ≤, ≥}.
        + **Valutazione della formula F**
            - Data una formula booleana F e una tupla t, per determinare se F(t) è vera:
                * Per ogni predicato in F:
                    + A θ c è vero per t se t[A] è in relazione θ con c (es: A ≠ c è vero se t[A] ≠ c)
                    + A θ B è vero per t se t[A] è in relazione θ con t[B] (es: A ≥ B è vero se t[A] ≥ t[B])

            ![alt text](./res/sel-es-1.png "Esempio selezione.")

    * **Proiezione**
        + Permette di selezionare un sottoinsieme Y degli attributi di una relazione.
        + La cardinalità di πY(r) è minore o uguale della cardinalità di r (la proiezione "elimina i duplicati"). 
        + L'uguaglianza è garantita se e solo se Y è una superchiave di R(X).

        ![alt text](./res/pro-es-1.png "Esempio proiezione.")

    * **Join naturale** (inner join)
        + Combina le tuple di due relazioni sulla base dell'uguaglianza dei valori degli attributi comuni.

        ![alt text](./res/es-join-nat-1.png "Esempio join naturale.")

        + E' commutativo e associativo
        + Se una tupla di una delle relazioni (operandi) non fa match con nessuna tupla dell'altra relazione è detta "dangling", è quindi possibile che il risultato del join sia vuoto, quindi la cardinalità può essere 0 ≤ |r1\><r2| ≤ |r1| ∗ |r2|.
        + Se il join è eseguito su una superchiave di R1(X1), allora ogni tupla di r2 fa match con al massimo una tupla di r1, quindi |r1\><r2| ≤ |r2|.
        + Se X1 ∩ X2 è la chiave primaria di R1(X1), e foreign key in R2(X2) (e quindi vi è un vincolo d'integrità referenziale) allora |r1\><r2| = |r2|. Questa affermazione è vera in assenza di valori nulli.
        + Quando le due relazioni hanno lo stesso schema (X1=X2) allora due tuple fanno match se e solo se hanno lo stesso valore per tutti gli attributi, ovvero sono identiche, per cui se X1 = X2 il join naturale equivale all'intersezione (∩) delle due relazioni.

            ![alt text](./res/es-join-int.png "Esempio join naturale intersezione.")

        + Se non vi sono attributi in comune (X1 ∩ X2 = ∅) allora, due tuple fanno sempre match, per cui se X1 ∩ X2 = ∅ il join naturale equivale al prodotto cartesiano (chiamato cross product/cross join).

            ![alt text](./res/es-join-int-1.png "Esempio join naturale intersezione.")

    * **Unione e differenza**

        ![alt text](./res/es-uni-e-dif.png "Esempio unione e differenza.")

    * **Ridenominazione**
        + Modifica lo schema di una relazione, cambiando i nomi di uno o più attributi.

        ![alt text](./res/es-ride.png "Esempio di ridenominazione.")

- **Operatori derivati**
    * **Intersezione**
        + Si può esprimere anche tramite la differenza: r1 ∩ r2 = r1 – (r1 – r2); quindi è un operatore derivato.

        ![alt text](./res/es-int.png "Esempio intersezione.")

        + **Self-join**: join di una relazione con se stessa.

            ![alt text](./res/es-self-join.png "Esempio self join.")
    
    * **Divisione** (÷)
        + La divisione di r1 per r2, con r1 su R1(X1X2) e r2 su R2(X2), è il più grande insieme di tuple t ∈ πX1(r1), e dunque con schema X1, tale che, facendo il prodotto cartesiano con r2, ciò che si ottiene è una relazione contenuta in r1 o uguale a r1.
        + Si può esprimere come: πX1(R1) − πX1((πX1(R1)><R2) − R1).

        ![alt text](./res/es-div.png "Esempio divisione.")

    * **Theta-join** (><F) (inner join)
        + Combinazione di prodotto cartesiano e selezione.
        
        ![alt text](./res/es-theta-join.png "Esempio theta join.")

        ![alt text](./res/precisazione-theta-join.png "Precisazione theta join.")

    * **Semi-join** (><>) vedi immagini per giusta simbologia
        + Da S a R è la proiezione del natural join R><S sugli attributi dello schema R.

            ![alt text](./res/esp-semi-join.png "Simbologia semi-join.")

        + E' utile in ambiente distribuito in quanto, se r ed s sono su nodi diversi della rete, consente di ridurre la mole dei dati da trasferire; infatti vale la seguente proprietà: 
            - (r><>s)><s = (s><>r)><r = r><s
        + Non è simmetrico: (r><>s) ≠ (s><>r)
- **Valori nulli**
    * Proiezione, unione e differenza continuano a comportarsi usualmente, quindi due tuple sono uguali anche se ci sono dei valori NULL.
    * **Selezione**
        + **Logica a tre valori**
            - Oltre ai valori di verità Vero (V) e Falso (F), si introduce il valore "Sconosciuto" (?).

            ![alt text](./res/vfn.png "Tabella di verità logica a tre valori.")

            - Una selezione produce le sole tuple per cui l'espressione di predicati risulta vera.
            - Per operare esplicitamente con i valori NULL si introduce l'operatore di confronto IS, ad esempio: A IS NULL.
            - NOT(A IS NULL) si scrive anche A IS NOT NULL.

        ![alt text](./res/es-sel-nul.png "Esempio selezione con valori nulli.")

    * **Join naturale**

        ![alt text](./res/es-join-null.png "Esempio join con valori nulli.")

    * **Intersezione**
        + In assenza di valori nulli l'intersezione di r1 e r2 si può esprimere in due modi:
            - r1><r2
            - r1 − (r1 − r2)
        + In presenza di valori nulli:
            - r1><r2 = non contiene tuple con valori nulli
            - r1 − (r1 − r2) = contiene tuple con valori nulli

        ![alt text](./res/es-nul-int.png "Esempio intersezione con valori nulli.")

    * **Outer join**
        + Join naturale che comprende nel risultato anche le tuple dangling.
        + Tipi:
            - **Left** (=><): sono incluse solo le tuple dangling dell'operando sinistro, e completate con NULL.
            - **Right** (><=): come sinistro ma a destra.
            - **Full** (=><=): Left + Right

        ![alt text](./res/es-full-out-join.png "Esempio full outer join.")

- **Espressioni**
    * Tipi:
        + **Lineare**
        + **Albero**: bottom-up

        ![alt text](./res/es-lin-vs-alb.png "TEsempio espressioni.")

    * **Viste**: espressioni a cui viene assegnato un nome e che è possibile riutilizzare all'interno di altre espressioni.
        + **Sintassi**: V:=E 
            - V = nome della vista
            - E = espressione

        ![alt text](./res/es-viste.png "Esempio viste, espressioni.")

    * Esempi

        ![alt text](./res/db-esempio.png "Database di prova per gli esempi.")

        ![alt text](./res/es-esp-1.png "Esempio di espressioni 1.")

        ![alt text](./res/es-esp-2.png "Esempio di espressioni 2.")

    * **Equivalenze**: due espressioni E1 ed E2 espresse su un database con schema DB si dicono equivalenti rispetto a DB (E1≡(DB)E2) se e solo se per ogni stato db producono lo stesso risultato, E1(db)=E2(db).
        + In alcuni casi l'equivalenza non dipende dallo schema DB specifico, nel qual caso si scrive E1≡E2.
        + Due espressioni equivalenti E1 ed E2 garantiscono lo stesso risultato, ma ciò non significa che la scelta sia indifferente in termini di "risorse" necessarie, è necessario analizzare quale sia la più prestante.
        + **Regole**
            - Il join naturale è commutativo e associativo
            - Selezione e proiezione si possono raggruppare

                ![alt text](./res/rag-sel-pro.png "Esempio di raggruppamento di selezione e proiezione.")

            - Selezione e proiezione commutano
            - "Push-down" della selezione rispetto al join, utile perchè puoi prima selezionare e poi fare le altre operazioni alleggerendo il tutto.

                ![alt text](./res/push-down.png "Esempio di push down.")

- **Ulteriori estensioni**
    * **Proiezione generalizzata**

        ![alt text](./res/es-pro-gen.png "Esempio di proiezione generalizzata.")

    * **Funzioni aggregative**
        + **avg**
        + **min**
        + **max**
        + **sum**
        + **count**
    * **Raggruppamenti**

        ![alt text](./res/es-rag-1.png "Esempio di raggruppamento.")

        ![alt text](./res/es-rag-2.png "Esempio raggruppamento, risultato.")