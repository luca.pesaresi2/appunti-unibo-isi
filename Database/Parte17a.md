### Parte 17 a - Query plan cardinality and histogram selectivity

- **RDBMS optimizer**
    * **Funzionamento**
        1. Riscrittura della query ottimizzata
        2. Selezione del quey execution plan di costo minimo
        3. Generazione del codice
- **Query plans**
    * **Logical**

        ![alt text](./res/es-logical-qp.png "Esempio di logical query plan.")

    * **Physical**

        ![alt text](./res/es-physical-qp.png "Esempio di physical query plan.")

- **Operatori**
    * **Logici**: sono in SQL un insieme esteso rispetto a quello dell'algebra relazionale (es: update, join). Ciascun operatore logico svolge una determinata funzione e produce in uscita un set o un multiset di record con certe proprietà.
    * **Fisici**
        + Implementazioni fisiche dei corrispondenti operatori logici; un operatore logico può avere più implementazioni diverse (es: merge join e nested loops join sono due possibili implementazioni del join)
        + Implementazioni di metodi di accesso a dati di una tabella o di un indice (es: table fullscan, index scan).
        + E' possibile associare ad ogni operatore fisico un costo di esecuzione in termini di operazioni di I/O e di operazioni di CPU.
- **Query execution plan**
    * Sequenza ordinata di passi d'esecuzione per generare il risultato della query.
    * Ogni passo corrisponde a un operatore fisico.
    * L'esecuzione è valutata dal basso verso l'alto.
    * **Strategie**
        + **Materializzazione**: ogni operatore memorizza il proprio risultato in una tabella temporanea; un operatore deve attendere che gli operatori di input da cui dipende abbiano terminato l'esecuzione per iniziare a produrre il proprio risultato.
        * **Pipeline**: ogni operatore richiede un risultato agli operatori di input e può essere non-bloccante o bloccante.
- **Count distinct problem**
    * Istanza: è dato uno stream di elementi x1, x2, ..., xs con ripetizioni; sia n il numero di elementi distinti e {y1, y2, ..., yn} il corrispondente insieme.
    * Obiettivo: fornire una stima n' della cardinalità n =|{y1, y2, ..., yn}| usando solo m<<n unità di memoria.
    * **Linear counting**
        + Fa uso di:
            - Bit map BM in memoria centrale di dimensione B bit.
                * Inizializzata ponendo tutti i bit a 0.
            - Funzione hash H a valori in [0,B-1]. 
        + Svolgimento:
            - Per ogni tupla r si applica all'attributo d'interesse A una funzione hash H a valori in [0,B-1] e si pone BM[H(r.A)] = 1.
            - Dopo aver elaborato in questo modo tutte le tuple, si conta il numero, Z, di bit a 0 presenti nella bit-map. 
        + Valore stimato di valori distinti: $`-B \times ln(\frac{Z}{B})`$
        + Per evitare che la bit map si saturi (per numero elevato di record): $`B \ge 0.2 \times NRdistinti`$
            - Approccio conservativo: $`B \ge 0.2 \times NR`$
            - Approccio aggressivo: usa un'approssimazione scadente di NRdistinti, come primo tentativo, e ripete il procedimento se la bit map satura.
    * **HyperLogLog**
        + S multiset.
        + H funzione hash che mappa in[0, $`2^{L}-1`$].
        + Trasformo ogni elemento di S in una stringa binaria di L bit.
        + Se la funzione hash è buona il multiset S risulta trasformato in un multiset SH di numeri interi non negativi distribuiti random uniformemente.
        + Se esaminando tutte le stringhe binarie generate si osserva che il massimo numero di leading zeros è k-1, allora una stima iniziale per il numero di valori distinti di SH è paria $`2^{k}`$.
        + Stima dei valori distinti finale: $`\frac{2^{k}}{φ}`$ con φ = 0.777351.....
- **Query selectivity estimation**: stimare il numero di record  del risultato dell'esecuzione di una query.
    * Stimare la selettività dei predicati è indispensabile per valutare la convenienza di un piano d'accesso e determinarne il costo.

        ![alt text](./res/predicati.png "Esempio di predicati.")

    * **Query su singola relazione**
        + Relazione: R
        + Numero record: $`NR_{R}`$ 
        + Query: $`q_{R}`$
        + Fattore di selettività: $`0 \le f_{qR} \le 1`$ 
        + Expected records (numero record di risultato): $`ER_{R,q} = f_{qR} \times NR_{R}`$ 
    * **Forma normale congiuntiva**
        + Durante l'ottimizzazione la clausola WHERE di una query è posta in forma normale congiuntiva, ovvero predicati semplici connessi da and e or, e rappresentata internamente con un albero di predicati, mettendo in evidenza i fattori booleani.
        + Esempio
            - EMPLOYEES(EmpNo,LastName,FirstName,Sex,Salary,Job,DepNo,...)
            - ```
                SELECT  EmpNo, LastName, FirstName, Salary 
                FROM    EMPLOYEES 
                WHERE   DeptNo=51
                AND     Salary>10000
                AND     (Job='Clerk' OR Sex ='Female')
              ```
            
                ![alt text](./res/form-norm-cong.png "Esempio di logical query plan.")
            
        + I fattori booleani che possono essere risolti via indice sono detti predicati di ricerca, gli altri predicati residui.
    * **Selettività predicati locali**
        + **Caso generico**
            - Predicato: p(R.A)
            - Tuple: $`NR_{R}`$
            - Valori di chiave distinti attesi: $`EK_{p(R.A)}`$
            - Valori distinti iniziali: $`NK_{p(R.A)}`$
            - Selettività: $`f_{p(R.A)} = \frac{EK_{p(R.A)}}{NK_{p(R.A)}}`$
            - Tuple residue: $`ER_{R,p(R.A)} = f_{p(R.A)} \times NR_{R}`$
        + **Casi notevoli**
            - Predicato `=`: $`f_{R.A=v} = \frac{1}{NK_{R.A}}`$
            - Predicato `IN`: $`f_{R.A ∈ set} = \frac{card(set)}{NK_{R.A}}`$
            - Predicato `<`:
                * Attributi numerici: $`f_{R.A<v} = \frac{v-min(R.A)}{max(R.A) - min(R.A)} \times \frac{NK_{R.A} - 1}{NK_{R.A}}`$
                * Attributi con molti valori: $`f_{R.A<v} = \frac{v-min(R.A)}{max(R.A) - min(R.A)}`$
                * Attributi non numerici: $`f_{R.A<v} < \frac{1}{2}`$
            - Predicato `BETWEEN`:
                * Attributi numerici: $`f = \frac{v_{2} - v_{1}}{max(R.A) - min(R.A)} \times \frac{NK_{R.A} - 1}{NK_{R.A}} + \frac{1}{NK_{R.A}}`$
                * Attributi con molti valori: $`f = \frac{v_{2} - v_{1}}{max(R.A) - min(R.A)}`$
                * Attributi non numerici: $`f = \frac{1}{3}`$
        + **Selettività di una composizione di predicati**
            - $`p = p_{1} and p_{2}`$ -> $`f_{p} = f_{p1} \times f_{p2}`$
            - $`p = p_{1} or p_{2}`$ -> $`f_{p} = f_{p1} + f_{p2} − f_{p1} \times f_{p2}`$
            - $`p = not p_{1}`$ -> $`f_{p} = 1 - f_{p1}`$
        + Esempi
            - $`NP_{EMPLOYEES}`$ = 2000
            - $`NR_{EMPLOYEES}`$ = 20000
            - $`NK_{DeptNo}`$ = 100
            - $`NK_{Job}`$ = 10
            - $`NK_{Sex}`$ = 2
            - $`NK_{Salary}`$ = 10 
            - min(DeptNo) = 1
            - max(DeptNo) = 100
            - min(Salary) = 5000
            - max(Salary) = 50000

            | Predicato | Fattore di selettività | Tuple residue |
            | ---------- |:-----------:| --------:| 
            | DeptNo = 51 | 1/100         | 200    |
            | Salary>10000  | [(50000- 10000)/( 50000- 5000)]×9/10=(8/9  )×(9/10)=8/10         | 16000    |
            | job=‘Clerk’     | 1/10         | 2000     |
            | Sex=‘Female’   | 1/2        | 10000     |

            | Predicato composto | Fattore di selettività | Tuple residue |
            | ---------- |:-----------:| --------:| 
            | DeptNo = 51 and Salary > 10000| 1/100 × 8/10 = 8/1000     | 160    |
            | job=‘Clerk’ or Sex=‘Female’ | 1/10 + 1/2 - (1/10) × (1/2) = 11/20 | 11000|

            - Il numero stimato di record restituiti da q1 è pari a 0.01× 0.8 × (11/20) × 20000 = 88
    * **Costi operatori**
        + **Selezione** (accesso sequenziale)
            - Data una tabella R: $`C_{I/O}(seqR) + C_{CPU}(seqR) = NP_{R} + α \times NR_{R}`$
                * **Scrittura**: Selezione + EP pagine di scrittura
                * **Proiezione**: Selezione + CPU
                    + Con eliminazione di duplicati: Proiezione + eventuali costi di I/O e CPU
    * **Costi query plan**
        + In generale: $`C(op) = C_{I/O}(op) + C_{CPU}(op) dove op indica l'implementazione di un access method o un logical operator
        + Nella esposizione di un query plan i costi evidenziati in ogni nodo dell'albero sono cumulativi.
        + **Notazione**
            - C(seqR) per table full scan
            - C(IX(R.A) uncl) per unclustered index scan
            - C(IX(R.A) clus) per clustered index scan
            - C(sort(NP)) per l'ordinamento esterno di un file di NP pagine.
- **Notazione per modello di costo**
    * R: relazione
    * $`NP_{R}`$: numero di pagine di R
    * $`NR_{R}`$: numero di record di R
    * R.A: generico attributo di R
    * $`q_{R}`$: query su R
    * $`f_{qR}`$: fattore di selettività della query $`q_{R}`$
    * $`ER_{R,q}`$ (Expected Records from $`q_{R}`$) = $`f_{qR} \times NR_{R}`$: numero di record del risultato della query $`q_{R}`$
    * p(R.A): predicato su R.A presente in $`q_{R}`$
    * $`NK_{R.A}`$: numero di valori distinti dell'attributo R.A
    * $`EK_{p(R.A)}`$ (Expected distinct Key values from p(R.A)): numero di valori distinti dell'attributo R.A selezionati dal predicato p(R.A)
    * $`f_{p(R.A)} = \frac{EK_{p(R.A)}}{NK_{R.A}}: fattore di selettività del predicato p(R.A)
    * $`ER_{R,p(R.A)}`$ (Expected Records from p(R.A)) = $`f_{p(R.A)} \times NR_{R}`$: numero di record che soddisfano p(R.A)
    * $`EP_{R,q}`$ (Expected result Pages from q): numero di pagine per contenere il risultato
    * IX(R.A): indice di tipo B+-tree costruito sull'attributo R.A
    * $`h_{IX(R.A)}`$: altezza dell'indice IX(R.A)
    * $`NL_{IX(R.A)}`$: numero di foglie dell'indice IX(R.A)
    * $`EL_{p(R.A)}`$ (Expected Leaves of IX(R.A)): numero di foglie dell'indice IX(R.A) a cui si deve accedere per reperire i RID dei record che soddisfano il predicato p(R.A)
    * $`EN_{p(R.A)}`$ (Expected Nodes of IX(R.A)): numero nodi di IX(R.A) visitati complessivamente escludendo le foglie
        + $`EN_{p(R.A)} = h_{IX(R.A)} - 1`$ in presenza di predicato di uguaglianza o di range
    * $`EP_{p(R.A)}`$ (Expected data Pages retrieved from IX(R.A)): numero di pagine dati reperite via indice IX(R.A)
    * $`RP_{R} = \frac{NR_{R}}{NP_{R}}`$: numero medio di record per pagina
    * $`D_{R}`$: dimensione in byte di una pagina di R
    * $`D_{R}*`$: spazio in byte utilizzabile in una pagina per i record
    * $`u_{R}`$: utilizzazione media di una pagina dati
    * $`u_{IX(R.A)}`$: utilizzazione media di una foglia dell'indice IX(R.A)
    * $`L_{R}`$: occupazione in byte di un record di R
    * $`L_{R.A}`$: occupazione in byte del generico attributo R.A
    * $`L_{R,q}`$: occupazione in byte complessiva del record restituito dalla query q
    * $`L_{RID}`$: occupazione in byte di un RID
    * FS (Frame Size): dimensione in pagine di un frame buffer
    * B: numero di pagine buffer per il sort organizzate in frame ciascuno di FS pagine
    * $`ER_{B}`$: numero medio di record che l'area di buffer ospita durante il sort
    * Z: numero di vie dell'algoritmo Z-way Sort-Merge
    * α: coefficiente per tener conto del costo di CPU per elaborare un record restituito dal metodo d'accesso (valori tipici da 0.01 o 0.3).