### Parte 10 - Funzionalità e architetture del DBMS

- **Gestire la concorrenza**
    * Un DBMS deve garantire che gli accessi ai dati, da parte di diverse applicazioni, non interferiscano tra loro violando vincoli di integrità e alterando la consistenza del DB.
    * **Modello di transazione**
        + Una transazione è vista come una sequenza di operazioni elementari di lettura (R) e scrittura (W) di oggetti del DB che, a partire da uno stato iniziale consistente del DB, porta il DB in un nuovo stato finale consistente, passandro per stati non necessariamente consistenti.
        + **Terminazione di una transazione**
            - **Corretta**: ciò avviene solo quando l'applicazione, dopo aver effettuato tutte le proprie operazioni, esegue una particolare istruzione, detta COMMIT, che comunica "ufficialmente" al modulo Transaction Manager del DBMS il termine delle operazioni.
            - **Non corretta**
                * La transazione decide che non ha senso continuare e quindi "abortisce" (ROLLBACK).
                * Il sistema non è in grado (es: violazione di un vincolo) di garantire la corretta prosecuzione della transazione, che è quindi fatta abortire (UNDO)
        + Nei sistemi SQL è possibile definire dei "savepoint"   che sono utilizzati da una transazione per disfare solo parzialmente il lavoro svolto.
- **RDBMS: Proprietà ACID**
    * **A**TOMICITY: la transazione è indivisibile nella sua esecuzione che deve essere o totale o nulla.
    * **C**ONSISTENCY: quando inizia una transazione il database si trova in uno stato consistente e al termine della transazione deve ancora essere in uno stato consistente.
    * **I**SOLATION: ogni transazione deve essere eseguita in modo isolato e indipendente dalle altre transazioni, l'eventuale fallimento di una transazione non deve interferire con le altre transazioni in esecuzione.
        + Utilizzata acquisizione e rilascio di lock tramite appropriati protocolli (es: Strict two-phase locking); si può verificare deadlock.
    * **D**URABILITY: persistenza; al termine della transazione i dati devono essere memorizzati in un dispositivo durevole e deve essere registrato il completamento della transazione.
        + Vengono impiegati log per tenere traccia delle transazioni.
- **Gestione dei guasti**
    * **Tipi**
        + **Transaction failure**: una transazione abortisce. L'interruzione può essere causata da una situazione prevista dal programma (es: violazione vincoli di integrità, tentativo di accesso a  dati protetti) o rilevata dal gestore (deadlock). L'interruzione non comporta perdita di dati.
        + **System failure**: interruzione di transazioni attive derivante da un'anomalia hardware o software dell'unità centrale o di una periferica (interruzione dell'alimentazione). Solo il contenuto della memoria permanente sopravvive.
        + **Media failure**: il contenuto (persistente) della base di dati risulta danneggiato.
        + **Communication failure**: nel caso di sistemi distribuiti si devono considerare anche guasti per partizionamento della rete e per perdita di messaggi.
    * **Strumenti**
        + **Database Dump**: copia di archivio del DB.
        + **Log**: file in cui sono registrate le operazioni di modifica eseguite dalle transazioni (WAL Protocol = si scrive prima sul log e poi sul DB).
- **Architettura**
    * **Categorie**
        + **Centralizzata**: tutte le funzionalità eseguite su una singola macchina.
        + **File Server**: l'elaborazione avviene nelle varie workstation dotate di DBMS e applicazioni.
        + **Client Server**
            - **Two-tier**: le componenti software sono distribuite su due sistemi: client e server. Presenta vari problemi:
                * scarsa scalabilità con centinaia-migliaia di utenti.
                * amministrazione onerosa dei client ad esempio per l'installazione di aggiornamenti alla logica applicativa o di nuove applicazioni.
                * thick client richiedono una configurazione ricca in termini di risorse (CPU,RAM,...) per soddisfare esigenze di efficienza nell'esecuzione delle transazioni.
            - **Three-tier**: come two-tier ma con un livello intermedio.
                * **Layers**
                    + **Presentation Layer**: si occupa dell'interfaccia con l'utente.
                    + **Business Logic Layer**: gestisce regole e vincoli intermedi prima di trasferire risultati all'utente o trasmettere dati al sottostante livello DBMS.
                    + **Database Services Layer**: è responsabile dei servizi di accesso al DB.
                * **Pro**
                    + Costi ridotti per thin client che richiedono minori risorse in terminidi hw.
                    + La logica applicativa è centralizzata in un singolo application server, spesso un web-server; in questo caso di psarla di WIS (Web information system).
                    + È garantita una maggiore modularità in quanto è più semplice sostituire un livello senza incidere sugli altri.
                    + È più semplice effettuare un bilanciamento del carico di lavoro grazie alla netta separazione tra logica applicativa e funzionalità database.
            - **N-tier**
                * **Middleware**: insieme di moduli software che svolgono il ruolo di intermediari tra diverse applicazioni e componenti software e/o realizzano l'integrazione dei processi e dei servizi, residenti su sistemi con tecnologie e architetture diverse.
        + **Peer to peer**: non vi sono né client né server e i vari siti possono dinamicamente creare nuove relazioni client-server.
- **DDBMS - Distributed Data Base Management System**
    * Un DDB non è meramente una "collezione di DB", tra i dati memorizzati sui diversi nodi deve esistere una correlazione logica; ciò si riflette nella presenza di "applicazioni distribuite" che elaborano dati allocati su più nodi.
    * **Transazioni multi-processo**
        + Una transazione è, nel caso generale, costituita da sottotransazioni eseguite da vari processi che possono anche operare su macchine diverse. In questo caso è necessario evitare che ogni singola sottotransazione termini o abortisca separatamente.
        + **Two-Phase Commit** (2PC)
            - Corretto a fronte di qualsiasi tipo di malfunzionamento, inclusi i guasti di rete e la perdita di messaggi. Restano esclusi i casi in cui si perdono i Log.
            - **Fasi**
                1. Il coordinatore verifica la possibilità dei partecipanti di eseguire commit.
                2. Sulla base delle riposte dei partecipanti decide sull'esito finale della transazione.
            - Un partecipante si impegna a eseguire commit appena sa che anche gli altri sono d'accordo.
            - Bloccante in caso di fallimento del coordinatore.
        + **Three-Phase Commit** (3PC)
            - Un partecipante può eseguire commit solo se sa che anche gli altri sanno della decisione comune. Si rende quindi necessario un terzo round di messaggi.
            - In caso di fallimento del coordinatore e transazione da terminare normalmente, i partecipanti devono eleggere un nuovo coordinatore.
- **DBMS Parallelli**
    * **Architetture multi-processore**

        ![alt text](./res/par.png "DBMS parallelli")

    * **Architetture ibride**
- **Cloud database**
    * I servizi cloud si prendono cura di garantire una soddisfacente scalabilità dell'architettura hw/sw e un'elevata disponibilità della base di dati. Essi rendono lo stack del software sottostante trasparente all'utente.
    * **Gestione**
        + **Virtual machine image**: gli utenti possono acquistare istanze di macchine virtuali.
        + **Database-as-a-service** (DBaaS): il fornitore del servizio si prende la responsabilità di installare e manutenere il database.