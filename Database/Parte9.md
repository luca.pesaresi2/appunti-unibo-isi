### Parte 9 - Normalizzazione

- Una **forma normale** è una proprietà di uno schema relazionale che ne garantisce la "qualità", cioè l'assenza di determinati difetti.
- Una relazione non normalizzata:
    * presenta ridondanze
        + **Ridondanza concettuale**: informazioni che possono essere derivate da altre già contenute nel DB.
        + **Ridondanza logica**: duplicazioni sui dati (con "ridondanza" ci riferiremo a questa sottospecie).
            - Si può eliminare scomponendo gli schemi.
    * provoca anomalie
        + **Anomalia di aggiornamento**: necessità di estendere l'aggiornamento di un dato a tutte le tuple in cui esso compare.
        + **Anomalia di cancellazione**: l'eliminazione di una tupla può comportare l'eliminazione di dati che conservano comunque la loro validità.
        + **Anomalia di inserimento**: l'inserimento di informazioni relative a uno solo dei concetti di pertinenza di una relazione è impossibile se non esiste un intero insieme di concetti in grado di costituire una tupla completa.
- **Dipendenza funzionale**

    ![alt text](./res/dip-func.png "Esempio di dipendenza funzionale.")

    * Vale il concetto di legalità/illegalità.
    * X → Y = Y dipende da X
    * Se X → Y non necessariamente Y → X
    * Se K è una chiave in uno schema R(T) allora ogni altro attributo di R(T) dipende funzionalmente da K.
- **Forme normali**
    * **Classifica gerarchica delle forme normali**
        + 1NF (First Normal Form)
        + 2NF (Second Normal Form)
        + 3NF (Third Normal Form)
        + EKNF (Elementary Key Normal Form)
        + BCNF (Boyce–Codd Normal Form)
        + 4NF (Fourth Normal Form)
        + ETNF (Essential Tuple Normal Form)
        + RFNF (Redundancy Free Normal Form)
        + SKNF (SuperKey Normal Form)
        + 5NF (Fifth Normal Form)
        + 6NF (Sixth Normal Form)
    * **1NF**
        + **Definizione**
            - Il dominio di ciascun attributo comprende solo valori atomici (semplici, indivisibili) e il valore di ciascun attributo in una tupla è un valore singolo del dominio di quell'attributo.
            - Una tabella è in 1NF se:
                * Non c'è un ordinamento delle righe.
                * Non c'è un ordinamento delle colonne.
                * Non ci sono righe duplicate.
                * Ogni intersezione riga-colonna contiene solo un valore del dominio.
                * Le righe non hanno componenti nascosti (come ID riga, ID oggetto..)
            - Esempio: DIPARTIMENTI(K:CodDip, Nome, CodDir, SediDip)

                | K:CodDip | Nome            | CodDir | SediDip       |
                | -------- |:---------------:| ------:| -------------:|
                | D0001    | Amministrazione | 33301  | **(Milano, Napoli, Roma)**|
                | D0005    | Produzione      | 18007  | Aprilia       |
                | D0003    | Ricerca         | 33010  | Napoli        |

                * Non rispetta la 1NF perchè il dominio di SediDip contiene valori atomici ma alcune tuple hanno un insieme di questi valori.
        + **Normalizzazione**
            - DIPARTIMENTI(K:CodDip, Nome, CodDir)

                | K:CodDip | Nome            | CodDir |
                | -------- |:---------------:| ------:|
                | D0001    | Amministrazione | 33301  |
                | D0005    | Produzione      | 18007  |
                | D0003    | Ricerca         | 33010  |

            - SEDI(K:CodDip:DIPARTIMENTI, K:SedeDip)

                | K:CodDip | K:SedeDip  |
                | -------- |:----------:|
                | D0001    | Milano     |
                | D0001    | Napoli     |
                | D0001    | Roma       |
                | D0005    | Aprilia    |
                | D0003    | Napoli     |
    * **2NF**
        + **Attributo primo**: se fa parte di almeno una chiave dello schema. In caso contrario è detto non-primo.
        + **Definizione**
            - Ogni attributo non-primo dipende completamente (non parzialmente) da ogni chiave candidata dello schema.
            - Uno schema in 1NF le cui chiavi sono tutte formate da un singolo attributo è anche in  2NF.
            - Esempio: MAGAZZINI(K:Articolo, K:Magazzino, Quantità, Indirizzo)
                * Vincoli:
                    + Articolo, Magazzino → Quantità ,Indirizzo
                    + Magazzino → Indirizzo:
                        - Problema: ogni tupla memorizza informazioni individuate da un valore della chiave AM, ma l'indirizzo dipende solo parzialmente dalla chiave.

                | K:Articolo | K:Magazzino | Quantità | Indirizzo       |
                | ---------- |:-----------:| --------:| -------------:|
                | scarpe     | VR1         | 25000    | via Albere 17 - Verona |
                | pantaloni  | VR1         | 18000    | via Albere 17 - Verona |
                | scarpe     | BO1         | 4500     | via Agucchi 3 - Bologna|
                | camicie    | VR2         | 7000     | via Monti6-Verona          |
        + **Normalizzazione**
            - ARTICOLI_IN_MAGAZZINI(K:Articolo, K:Magazzino, Quantità)(AM → Q)

                | K:Articolo | K:Magazzino | Quantità |
                | ---------- |:-----------:| --------:|
                | scarpe     | VR1         | 25000    |
                | pantaloni  | VR1         | 18000    |
                | scarpe     | BO1         | 4500     |
                | camicie    | VR2         | 7000     |
            - INDIRIZZI_MAGAZZINI(K:Magazzino, Indirizzo) (M → I)
                    
                | K:Magazzino | Indirizzo              |
                |:-----------:| ----------------------:|
                | VR1         | via Albere 17 - Verona |
                | VR1         | via Albere 17 - Verona |
                | BO1         | via Agucchi 3 - Bologna|
                | VR2         | via Monti6-Verona      |
    * **3NF**
        + **Dipendenza transitiva**: tra A e X, A dipende da X tramite Y.
            - A dipende transitivamente da X se:
                * X → Y
                * ¬(Y → X)
                * Y → A
                * A ∉ Y
        + **Definizione**
            - Non c'è dipendenza transitiva di un attributo non-primo da una chiave.
            - Oppure: per ogni dipendenza funzionale non banale X → Y, X è una superchiave oppure ogni attributo A in  Y è contenuto in almeno una chiave, cioè A è un attributo primo.
            - 3NF implica 2NF.
            - Esempio: IMPIEGATI(K:IdImpiegato, Cognome, Nome, Agenzia, Luogo)
        + **Normalizzazione**
            - IMPIEGATI_AGENZIE(IdImpiegato, Cognome, Nome, Agenzia) (I → CNA)

                | K:IdImpiegato | Cognome | Nome   | Agenzia |
                | ------------- |:-------:| ------:| -------:|
                | 001           | Rossi   | Carlo  | 02400   |
                | 002           | Verdi   | Maria  | 53880   |
                | 003           | Bianchi | Giulia | 04826   |
                | 004           | Neri    | Franco | 23900   |
                | 005           | Gialli  | Marco  | 02400   |
            - AGENZIE(K:Agenzia,Luogo) (A → L)
                    
                | K:Agenzia | Luogo   |
                |:---------:| -------:|
                | 02400     | Bologna |
                | 53880     | Bergamo |
                | 04826     | Cagliari|
                | 23900     | Cesena  |
        + **Decomposizione**
            - Una decomposizione con perdita può generare tuple spurie.
            - **Senza perdita**
                * Uno schema R(X) si decompone senza perdita negli schemi R1(X1) e R2(X2) se, per ogni stato legale r su R(X), il join naturale delle proiezioni di r su X1 e X2 è uguale a r stessa: πX1(r) >< πX2(r) = r
    * **BCNF**
        + **Definizione**
            - Per ogni dipendenza funzionale (non banale) X → Y, X è una superchiave di R(T).
            - Esempio: ELENCO_TELEFONICO(K:Pref, K:Num, Località, Abbonato, TopCiv)
                * Vincoli:
                    + Pref, Num → Località, Abbonato, TopCiv (PN → LAT)
                    + Località → Pref (L→P):
                        - Problema: Pref è un attributo primo ma Località non è superchiave.
        + **Normalizzazione**
            - NUM_TEL(K:Num, K:Località, Abbonato, TopCiv)

                | K:Num  | K:Località | Abbonato  | TopCiv           |
                | ------ |:----------:| ---------:| ----------------:|
                | 432175 | Bologna    | Rossi M.  | Via Mazzini 124  |
                | 272225 | Modena     | Bianchi G.| Via Emilia 233   |
                | 227951 | Bologna    | Rossi M.  | Via Amendola 14  |
                | 314255 | Castenaso  | Neri E.   | Via Mazzini 7    |
                | 227951 | Vignola    | Verdi P.  | Piazza Roma 14   |
            - PREF_TEL(K:Località, Pref)
                    
                | Pref | K:Località |
                |:----:| ----------:|
                | 051  | Bologna    |
                | 059  | Modena     |
                | 051  | Castenaso  |
                | 059  | Vignola    |
            - Ma modificando il DB riscontriamo dei problemi (possono essere inseriti due numeri di telefono uguali, prefisso compreso)
        + **Preservazione delle dipendenze**
            - Ciascuna delle dipendenze funzionali dello schema originario coinvolge attributi che compaiono tutti insieme in uno degli schemi decomposti.
                * Nell'esempio Pref, Num → Località non è conservata.
        + **Query di verifica**: query da eseguire prima di eseguire una modifica.
            - Seguendo l'esempio precedente:
                * Bisogna verificare che la FD Pref, Num → Località sia conservata, a tal fine per inserire un nuovo abbonato occorre controllare che non esista nessun altro abbonato in una località con lo stesso prefisso di Modena che abbia lo stesso numero di telefono 227951.
                ```
                    SELECT  *  -- OK se non restituisce alcuna tupla
                    FROM    NUM_TEL N
                    WHERE   N.Numero = ‘227951’
                    AND     N.Località IN(SELECT    P2.Località
                                          FROM      PREF_TEL P1, PREF_TEL P2
                                          WHERE     P1.Pref = P2.Pref
                                          AND       P1.Località = `Modena’)
                ```
        + **Qualità di una decomposizione**
            - Senza perdita, per garantire la ricostruzione delle informazioni originarie.
            - Preservare le dipendenze, per semplificare il mantenimento dei vincoli di integrità originari.
        + **Algoritmo di decomposizione** (in 3NF)
            - Minimizzare l'insieme delle FD.
                * Se F è un insieme di FD su R(U) e X un insieme di attributi, quali attributi di U dipendono funzionalmente da X?
                    + Esempio: Supponiamo di avere F = {A → B, BC → D, B → E, E → C} e calcoliamo A+, ovvero l'insieme di attributi che dipendono da A
                        - A+ = A
                        - A+ = AB usando A → B
                        - A+ =ABE usando B → E
                        - A+ = ABEC usando E → C
                        - A+ = ABECD usando BC →D
                        - Quindi da A dipendono tutti gli attributi dello schema, ovvero A è superchiave.
                * In alcune FD è possibile che sul lato sinistro ci siano degli attributi inutili (estranei): devono essere eliminati.
                    + Come facciamo a stabilire che in una FD del tipo AX → B l'attributo A è estraneo? 
                        - Calcoliamo X+ e verifichiamo se include B, ovvero se basta X a determinare B.
                    + Esempio: 
                        - Supponiamo di avere F = {AB → C, A → B} e calcoliamo A+
                            * A+ = A
                            * A+ = AB poiché A → B e A ⊆ A+
                            * A+ = ABC poiché AB → C e AB ⊆ A+
                        - Quindi C dipende solo da A, ovvero in AB → C l'attributo B è estraneo (perché a sua volta dipendente da A) e possiamo riscrivere l'insieme di FD più semplicemente come: F’ = {A → C, A → B}
                * Si deve verificare se vi sono intere FD inutili (ridondanti), ovvero FD che sono implicate da altre.
                    + Esempio:
                        - F = {B → C, B → A, C → A}
                        - Si vede che B → A è ridondante e quindi possiamo riscrivere l'insieme di FD più semplicemente come: F’ = {B → C, C → A}
                    + Come facciamo a stabilire che una FD del tipo X → A è ridondante?
                        - La eliminiamo da F, calcoliamo X+ e verifichiamo se include A.
            - Creare una relazione per ogni gruppo di FD che hanno lo stesso lato sinistro e inserire nello schema corrispondente gli attributi coinvolti in almeno una FD del gruppo.
                * Esempio
                    + Schema: R(K:AB CDEFG)
                    + FD:
                        - AB → CD
                        - AB → E
                        - C → F
                        - F → G
                    + Si generano gli schemi:
                        - R1(K:AB CDE), R2(K:C F), R3(K:F G)
            - Se 2 o più determinanti si determinano reciprocamente, si fondono gli schemi (più chiavi alternative per lo stesso schema).
                * Esempio
                    + Schema: R(K:A BCD)
                    + FD:
                        - A → BC
                        - B → A
                        - C → D
                    + Si generano gli schemi:
                        - R1(K:A BC)
                        - R2(K:C D) 
                        - (con B chiave in R1)
            - Verifica che esista uno schema la cui chiave è anche chiave dello schema originario (se non esiste lo si crea).
                * Esempio
                    + Schema: R(K:A BCD)
                    + FD:
                        - A → C
                        - B → D
                    + Si generano gli schemi
                        - R1(K:A C)
                        - R2(K:B D)
                        - R3(K:AB)
            - Esempio generale (per altri Lez09, P82):
                * R(ABCDE)
                * F = {A → BC, B → C, ABE → D} 
                * Risoluzione
                    + Passo 1: F = {A → B, A → C, B → C, ABE → D}
                    + Passo 2: F’ = {A → B, A → C, B → C, AE → D}  poiché B dipende da A
                    + Passo 3: F’’ = {A → B, B → C, AE → D}  poiché A → C è ridondante
                    + Passo 4: Si generano gli schemi R1(K:A B), R2(K:B C) e R3(K:AE D)
                    + Passi 5 e 6: Le chiusure delle chiavi sono:
                        - A+ = ABC
                        - B+ = BC
                        - AE+ = ABCED che è quindi anche chiave dello schema non decomposto
    * **Approccio pratico alla normalizzazione**
        + Se la relazione non è normalizzata si decompone in 3NF, il che è sempre possibile.
        + Si verifica se lo schema ottenuto è anche in BCNF (se una relazione ha una sola chiave allora le due forme normali coincidono).
        + Se lo schema non è in BCNF è possibile:
            - Lasciare lo schema ottenuto così, gestendo le anomalie residue.
            - Decomporre in BCNF, predisponendo opportune query di verifica.
            - Rimodellare la situazione iniziale, al fine di permettere di ottenere schemi BCNF.
    * **Analisi associazioni n-arie**
        + Schema E/R (1):

            ![alt text](./res/as-n.png "Esempio di associazione n-aria.")

        + FD:
            - Studente → CorsoDiLaurea
            - Studente → Professore
            - Professore → Dipartimento
        + Schema: TESI(Studente, Professore, Dipartimento, CorsoDiLaurea)
            - Dipendenza transitiva Professore → Dipartimento = non in 3NF
        + Schema E/R (prima ristrutturazione):

            ![alt text](./res/as-n-1.png "Esempio di associazione n-aria 1.")

        + FD: 
            - Studente → CorsoDiLaurea (iscrizione)
            - Studente → Professore (per chi ha un relatore)
        + Schema: TESI(Studente, Professore, CorsoDiLaurea) 
            - E' ora in BCNF.
        + Schema E/R (ristrutturazione finale):

            ![alt text](./res/as-n-2.png "Esempio di associazione n-aria 1.")
    
    * **Leggere uno schema E/R in termini di FD**

        ![alt text](./res/fd-er.png "Esempio di schema E/R.")

        + FD:
            - K1 → A1, B1
            - K2 → A2, B2
            - K1 → K2, AR poiché max-card(E1,R) = 1
    * **È sempre opportuno normalizzare?**
        + Dipende dai casi, bisogna considerare vari aspetti:
            - normalizzare elimina le anomalie ma può appesantire l'esecuzione di certe operazioni.
            - la frequenza con cui i dati sono soggetti a modifica incide (relazioni "quasi statiche" danno un minor numero di problemi se non sono normalizzate).
            - la ridondanza presente in relazioni non normalizzate va quantificata al fine di comprendere quanto possa incidere sull'occupazione di memoria e sui costi derivanti dall'aggiornamento di repliche di una stessa informazione.