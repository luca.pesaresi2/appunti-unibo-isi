### Parte 7 - Progettazione logica

- **Obiettivo**: pervenire, a partire dallo schema concettuale (DBconc), a uno schema logico (DBrel) che rappresenti in modo fedele (i due schemi sono equivalenti dal punto di vista della loro capacità informativa) i concetti e i requisiti analizzati e che sia, al tempo stesso, "efficiente".
- **Come funziona?**
    * Questa attività di progettazione può essere vista come la definizione di un mapping M che spiega come trasformare ogni stato dbconc di DBconc in un corrispondente stato dbrel di DBrel.
    * M deve essere totale e iniettiva
        + **Totale**: per ogni stato dbconc di DBconc esiste uno stato dbrel di DBrel tale che M(dbconc) = dbrel.
        + **Iniettiva**: non esistono due stati db1conc e db2conc tali che M(db1conc) = M(db2conc).
- **Come procedere**
    * La traduzione da schema E/R a schema relazionale avviene operando una sequenza di trasformazioni/traduzioni semplici rispettando le regole che garantiscono l'equivalenza.
    * Tipi regole:
        + **Preservano l'informazione** (sulla struttura)
        + **Garantiscono l'equivalenza** (sui vincoli)
    * **Fasi**
        + **Ristrutturazione**
            - **Semplificazione**: eliminazione dallo schema E/R dei costrutti che non possono essere direttamente rappresentati nel modello relazionale:
                * Eliminazione degli attributi multivalore

                    ![alt text](./res/el-at-mul.png "Esempio di eliminazione di attributi multivalore 1.")

                    ![alt text](./res/el-at-mul-1.png "Esempio di eliminazione di attributi multivalore 2.")

                    ![alt text](./res/el-at-mul-2.png "Esempio di eliminazione di attributi multivalore 3.")

                    ![alt text](./res/el-at-mul-3.png "Esempio di eliminazione di attributi multivalore 4.")

                * Eliminazione delle gerarchie di generalizzazione
                    + **Collasso verso l'alto**: accorpare le entità figlie nel genitore

                        ![alt text](./res/col-ver-al.png "Esempio di collsaao verso l'alto.")

                        - **Tipo**: attributo selettore che specifica se una singola istanza di E appartiene a una delle N sotto-entità.
                            * **Copertura**
                                + **Totale esclusiva**: tipo assume N valori, quante sono le sotto-entità.
                                + **Parziale esclusiva**: tipo assume N+1 valori, il valore in più serve per le istanze che non appartengono a nessuna sotto-entità.
                                + **sovrapposta**: occorrono tanti selettori quante sono le sotto-entità, ciascuno a valore booleano Tipo_i, che è vero per ogni istanza di E che appartiene a E_i; se la copertura è parziale i selettori possono essere tutti falsi, oppure si può aggiungere un selettore.
                        - Le eventuali associazioni connesse alle sotto-entità si trasportano su E, le eventuali cardinalità minime diventano 0.
                    + **Collasso verso il basso**: accorpare il genitore nelle entità figlie.

                        ![alt text](./res/col-ver-bas.png "Esempio di collasso verso il basso.")

                        - Se la copertura non è completa non si può applicare poichè non si saprebbe dove collocare le istanze di E che non sono né in E1, né in E2.
                        - Se la copertura non è esclusiva introduce ridondanza; una certa istanza può essere sia in E1 sia in E2.
                    + **Sostituire la generalizzazione con associazioni**
                        
                        ![alt text](./res/es-sost-gen-as.png "Esempio di sostituzione di generalizzazioni con associazioni.")

                        - Tutte le entità vengono mantenute: le entità figlie sono in associazione binaria con l'entità padre e sono identificate esternamente.
                    + **Quale scegliere?**
                        - **Regole**
                            * Conviene se gli accessi all'entità padre e alle entità figlie sono contestuali?
                            * Conviene se gli accessi alle entità figlie sono distinti, ma d'altra parte è possibile solo con generalizzazioni totali?
                            * Conviene se gli accessi alle entità figlie sono separati dagli accessi al padre?
                        - E' possibile adottare soluzioni ibride.
                * Partizionamento/accorpamento di entità e associazioni
                    + **Gli accessi si riducono**:
                        - Separando attributi di un concetto che vengono acceduti separatamente.
                        - Raggruppando attributi di concetti diversi a cui si accede insieme.
                    + **Casi principali**
                        - Partizionamento
                            * Verticale di entità

                                ![alt text](./res/es-part-vert-ent.png "Esempio di partizionamento verticale di entità.")

                            * Orizzontale di associazioni

                                ![alt text](./res/es-part-or-as.png "Esempio di partizionamento orizzontale di associazioni.")

                        - Accorpamenti di entità e associazioni
                            * **Entità**

                                ![alt text](./res/acco-enti.png "Esempio di accorpamento di entità.")

                * Scelta degli identificatori principali
                    + **Criteri**
                        - Assenza di opzionalità (NULL)
                        - Semplicità
                        - Utilizzo frequente
                    + Se nessuno degli identificatori soddisfa i requisiti s'introducono nuovi attributi (codici) adhoc.
            - **Ottimizzazione delle prestazioni**
                * **Stima del carico di lavoro**
                    + **Principali operazioni** che la base di dati dovrà supportare.
                        - Tipi:
                            * **Interattiva** (I)
                            * **Batch** (B)
                        - **Frequenza**: numero medio di esecuzioni in un certo periodo di tempo.
                        - **Schema di navigazione**: frammento dello schema E/R interessato dall'operazione sul quale viene evidenziato (con frecce) il "cammino logico" da percorrere per accedere alle informazioni di interesse.

                            ![alt text](./res/es-val.png "Esempio di schema di navigazione.")

                        - **Tavola degli accessi** (E = entità, A = associazione, S = scrittura (costo = 2), L = lettura (costo = 1)).

                            ![alt text](./res/es-tav-ac.png "Esempio di espressioni 1.")

                    + **Volumi dei dati** tramite tabella dei volumi (E = entità, A = associazione).

                        ![alt text](./res/tav-vol.png "Esempio di tavola di volumi.")

                    + **Indicatori**
                        - **Spazio**: numero di istanze (di entità e associazioni) previste.
                        - **Tempo**: numero di istanze visitate durante un'operazione.
            - **Analisi delle ridondanze**
                * **Ridondanza**: informazione significativa ma derivabile da altre.
                * Se si mantiene una ridondanza:
                    + si semplificano delle interrogazioni
                    + si appesantiscono gli aggiornamenti
                    + si occupa maggior spazio
                * Le possibili ridondanze riguardano: 
                    + attributi derivabili da altri attributi

                        ![alt text](./res/es-at-rid.png "Esempio di attributi ridondanti.")

                    + associazioni derivabili dalla composizione di altre associazioni

                        ![alt text](./res/es-as-rid.png "Esempio di associazioni ridondanti.")

                * **Modus operandi**
                    + Si considerano le operazioni influenzate dalla ridondanza e le loro frequenze di esecuzione:
                        1. inserisci una nuova persona con la relativa città di residenza (500 volte al giorno)
                        2. visualizza tutti i dati di una città (incluso il numero di residenti) (2 volte al giorno)
                    + Si costruiscono le tavole degli accessi:
                        - con ridondanza

                            ![alt text](./res/es-tav-ac-rid.png "Esempio di tavola di accesso con ridondanza.")

                        - senza ridondanza

                            ![alt text](./res/es-tav-ac-norid.png "Esempio di tavola di accesso senza ridondanza.")

                    + Mantenere o no la ridondanza? Si poichè:
                        - Con ridondanza:
                            1. 1500 accessi in scrittura e 500 accessi in lettura al giorno
                            2. 2 accessi in lettura al giorno
                            * Totale: 3502 accessi al giorno
                        - Senza ridondanza
                            1. 1000 accessi in scrittura al giorno
                            2. 10002 accessi in lettura al giorno
                            * Totale: 12002 accessi al giorno
        + **Traduzione**: si mappano i costrutti residui in elementi del modello relazionale.
            - **Entità**

                ![alt text](./res/trad-ent.png "Esempio di traduzione di entità.")

            - **Associazioni**
                * Ogni associazione è tradotta con una relazione con gli stessi attributi, cui si aggiungono gli identificatori di tutte le entità che essa collega.
                * Gli identificatori delle entità collegate costituiscono una superchiave.
                * La chiave dipende dalle cardinalità massime delle entità nell'associazione.
                * Le cardinalità minime determinano la presenza o meno di valori nulli (e quindi incidono sui vincoli e sull'occupazione di memoria).

                ![alt text](./res/en-as-mo-mo.png "Esempio di traduzione di entità e associazione molti a molti.")

                * Non è necessario mantenere gli stessi nomi delle primary key referenziate.

                    ![alt text](./res/fk-nam.png "Specificazione immagine precedente.")

                * **Tipi**
                    + Ad anello molti a molti

                        ![alt text](./res/as-an-mo-mo.png "Esempio di associazione ad anello molti a molti.")

                    + N-arie molti a molti

                        ![alt text](./res/an-n-mo-mo.png "Esempio di associazione n-aria molti a molti.")

                    + Uno a molti

                        ![alt text](./res/as-un-mo.png "Esempio di associazione uno a molti.")

                    + Ad anello uno a molti

                        ![alt text](./res/as-an-un-mo.png "Esempio di associazione ad anello uno a molti.")

                    + Entità con identificazione esterna

                        ![alt text](./res/en-id-es.png "Esempio di associazione con entità con identificazione esterna.")

                        - A cascata: partire dalle entità non identificate esternamente e propagare gli identificatori che così si ottengono.

                            ![alt text](./res/en-id-es-ca.png "Esempio di associazione con entità con identificazione esterna a cascata.")

                    + Uno a uno

                        ![alt text](./res/as-1-1.png "Esempio di associazione uno ad uno con tre relazioni.")

                        ![alt text](./res/as-1-1-1.png "Esempio di associazione uno ad uno con due relazioni.")

                        ![alt text](./res/as-1-1-2.png "Esempio di associazione uno ad uno con una relazione.")

                        - Con opzionalità
                            * La traduzione con una sola relazione corrisponde a un accorpamento di entità:
                                + Se min-card(E1,R) = min-card(E2,R) = 1 si avranno due chiavi, entrambe senza valori nulli (la chiave primaria è la più importante).
                                + Se min-card(E1,R) = 0 e min-card(E2,R) = 1 la chiave derivante da E2 ammetterà valori nulli e la chiave primaria si ottiene da E1
                                + Se min-card(E1,R) = min-card(E2,R) = 0 entrambe le chiavi hanno valori nulli, quindi si rende necessario introdurre un codice
                        - Ad anello

                            ![alt text](./res/an-1-1.png "Esempio di associazione uno ad uno ad anello.")

- **Esempio finale**
    * **E/R**

        ![alt text](./res/er-tot.png "Esempio di schema e/r.")

    * **Ristrutturazione**

        ![alt text](./res/es-ris.png "Esempio di ristrutturazione completa.")

    * **Schema logico relazionale** (la prima è chiave primaria)
        + Entità che partecipano ad associazioni sempre con max-card(E,R) = n:
            - SEDI(Città, Via, CAP)
            - PROGETTI(Nome, Budget, DataConsegna)
        + Partecipazione:
            - PARTECIPAZIONI(Impiegato, Progetto)
            - FK: Impiegato REFERENCES Impiegati
            - FK: Progetto REFERENCES Progetti
        + L'entità Dipartimento si traduce importando l'identificatore di Sede e inglobando l'associazione Direzione:
            - DIPARTIMENTI(Nome, Città, Direttore)
            - FK: Città REFERENCES Sedi 
            - FK: Direttore REFERENCES Impiegati
        + L'entità Telefono si traduce con una relazione che ingloba l'associazione Recapito:
            - TELEFONI(Numero, Nome, Città)
            - FK: Nome, Città REFERENCES Dipartimenti
        + Per tradurre l'associazione Afferenza, assumendo che siano pochi gli impiegati che non afferiscono a nessun dipartimento, si opta per una rappresentazione compatta:
            - IMPIEGATI(Codice, Nome, Cognome, NomeDip*, CittàDip*, Data*)
            - FK: NomeDip, CittàDip REFERENCES Dipartimenti
