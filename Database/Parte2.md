### Parte 2 - Progettazione DB

- **Scenari**
    * **SI ex novo**: I task specifici relativi alle basi di dati devono essere coordinati con vari altri aspetti di design del SI.
    * **Sistema Inforamtico già in esercizio**: L'attività di progettazione è svolta come un processo a sé stante; non si escludono però modifiche e/o estensioni dell'architettura del sistema informatico.
- **Oggetti**
    * Ciò che esiste o è esistito o esisterà o potrebbe esistere, nella realtà o nell'immaginazione, di concreto o di astratto, di materiale o d'ideale (dunque una cosa, nel senso lato del significato, o un individuo, come singolo ente, in quanto distinto da altri della stessa specie, che "esiste nella nostra mente" ed è distinguibile da altri oggetti).
    * Esempi: lo studente Mario Rossi, il docente Dario Maio, il profumo del limone, il gatto Silvestro, la rubrica telefonica di Alice...
    * Può essere descritto a partire da termini molto generici come appartenente a una classe (esempio: un edificio), di cui successivamente si specificano le proprietà e i relativi vincoli (esempio: nome edificio, ubicazione, superficie...), e poi rappresentato in termini di valori che tali proprietà assumono (esempio: Palazzo Mazzini Marinelli, via Sacchi 3 Cesena, mq 4000...).
- **Funzioni**
    * Possono essere espresse inizialmente in modo vago (esempio: controllare il livello di gas nocivi nell'aria) e successivamente precisate (esempio: la programmazione del livello di soglia per l'allarme della centralina è attivata, dopo aver digitato un Pin, premendo il pulsante P).
- **Stati**
    * Possono essere descritti a un elevato livello di astrazione (esempio: la centralina è in stato di errore) e poi specificati in maggior dettaglio (esempio: è acceso il segnalatore d'errore nel sensore S).
- **Classi**
    * **Livello intensionale**
        + Una classe C denota in modo astratto un insieme U(C) di elementi che sono caratterizzati da proprietà comuni e che devono rispettare determinati vincoli, senza specificare concretamente i singoli elementi, ma avendo ben presente le caratteristiche che ciascuno di essi deve possedere per appartenere alla classe.
        + La definizione intensionale di una classe C rappresenta un concetto attraverso la dichiarazione di un tipo di elemento, descrivendo le caratteristiche che deve possedere ogni elemento dell'insieme che C rappresenta, con regole e/o costrutti che consentano la generazione dei suoi elementi.
        + Esempio: U(AUTOMOBILE): {(Targa: targa_eu, Alimentazione: tipo_alim, Colore: colore_auto, ...)}
            - Ogni elemento dell'insieme che la classe AUTOMOBILE rappresenta è una ennupla di valori di proprietà; ciascuna proprietà ha un nome (esempio: Alimentazione) e assume valori in un certo dominio (esempio: tipo_alim, definisce il dominio di valori {benzina,metano,GPL,gasolio,ibrida}).
            - Si deve specificare almeno un vincolo d'identificazione perché, per definizione d'insieme, tutti gli elementi devono essere distinti: Targa è identificatore.
            - Si possono definire inoltre vincoli di varia natura, ad esempio:
                * non è obbligatorio indicare il Colore
                * La massima emissione di HC dipende dal tipo d'alimentazione e dalla categoria EURO d'appartenenza
                * ...
    * **Livello estensionale**
        + Una classe C, osservata in un certo istante di tempo t, è costituita da un insieme popolato di elementi distinti E(C,t) = {e1, e2, e3...}, detto anche estensione di C al tempo t.
        + Ciascun elemento, detto istanza di C, è caratterizzato dai valori delle proprietà definite a livello intensionale.
        + E(C,t) è uno dei possibili sottoinsiemi dell'insieme U(C) definito intensionalmente, cioè E(C,t) ⊆ U(C); ovviamente un'istanza di E(C,t) è un'istanzadi U(C).
        + E(C,t) deve rispettare tutti i vincoli dichiarati nella definizione di C, in questo caso è detta "legale", altrimenti è "illegale".
        + Cardinalità = |E(C,t)| = |E(C)|
            - La cardinalità di una classe può essere parecchio variabile e i dati di un SI sono archiviati su memorie finite, per questo è cruciale, ai fini del buon funzionamento di un SI, la stima del volume dei dati e del conseguente carico di lavoro atteso in termini di interrogazioni, cancellazioni, inserimenti e modifiche.
        + Esempio: 
            - U(GATTO_REGISTRATO): {(Id: cod_g, CodProp: cod_fiscale, Nome: nome_g, Razza: razza_g...)}
            - E(GATTO_REGISTRATO,t):
                * (A001, RSSMRA65L22F839J, Briciolina, Siamese...)
                * (A002, BRNCRL82L70A944K, Fiocco, Persiano...)
                * ...
- **Meccanismi di astrazione**
    * **Classificazione**: consente di raggruppare in classi oggetti, funzioni, o stati in base al loro significato, alle loro caratteristiche e ai vincoli che devono essere soddisfatti.
    * **Generalizzazione**: cattura le relazioni "è un" ovvero permette di astrarre le caratteristiche comuni fra più classi definendo superclassi.
        + NB (per la generalizzazione in E/R)
            - Classe = Entità
            - Sottoclasse = Entità figlia
            - Superclasse = Entità madre/genitore
        + Ogni sottoclasse eredita proprietà dalla superclasse ma può anche avere caratteristiche proprie.
        + Affermare che l'istanza s di STUDENTE è anche un'istanza p di PERSONA significa che concentriamo l’attenzione sulle proprietà condivise definite dalla superclasse.
        + La specializzazione è il processo d'astrazione inverso rispetto alla generalizzazione.
        + **Copertura delle generalizzazioni**
            - Confronto fra unione delle specializzazioni e classe generalizzata:
                * Totale: classe generalizzata = unione delle specializzazioni.
                * Parziale: classe generalizzata ⊃ unione delle specializzazioni.
            - Confronto fra le calssi specializzate:
                * Esclusiva: gli insiemi delle specializzazioni sono fra loro disgiunti.
                * Sovrapposta: può esistere un'intersezione non vuota fra insiemi delle spezializzazioni.
            - Sono possibili le 4 combinazioni:
                * Totale - Esclusiva
                * Parziale - Esclusiva
                * Totale - Sovrapposta
                * Parziale - Sovrapposta
    * **Aggregazione**: esprime le relazioni "parte di" che sussistono tra oggetti, tra funzioni, o tra stati.
        + Esempio: il codice fiscale è "parte di" ogni istanza p di PERSONA...
    * **Proiezione**: cattura la vista delle relazioni strutturali fra gli oggetti, le funzioni, gli stati.
        + Esempio: nel descrivere il funzionamento di un certo PC può essere necessario distinguere il punto di vista dell'utente, dell’installatore, del programmatore.
- **Associazioni**
    * Sono corrispondenze tra classi, aggregazioni di cui le classi sono le componenti. Un'istanza di un'associazione è una combinazione di istanze delle classi che prendono parte all'associazione.
    * **Livello**
        + **Livello intensionale**
            - Si può definire A pensando genericamente alla possibilità di stabilire legami tra elementi di U(C1) ed elementi di U(C2), e dichiarando anche i vincoli di cardinalità min e max di partecipazione in A di una generica istanza di E(C1) e di E(C2).
        + **Livello estensionale**
            - In un certo istante di tempo, un'associazione A tra le classi C1 e C2 è costituita da un insieme E(A) di coppie(x,y), tali che x è una istanza di E(C1), ed y è una istanza di E(C2). 
            - Ogni coppia è detta istanza di E(A) o più brevemente istanza di A.
            - L'insieme E(A) così definito, è detto estensione di A, e rappresenta uno dei possibili sottoinsiemi dell'insieme U(A) definito intensionalmente. 
            - E(A) è un sottoinsieme del prodotto cartesiano E(C1) × E(C2) che rispetta i vincoli di cardinalità (estensione "legale"). Si dice "illegale" un'estensione che non rispetta i vincoli.
    * **Vincoli di cardinalità**
        + min-card(C1, A): cardinalità minima di C1 in A
            - partecipazione opzionale: min-card(C1, A) = 0
            - partecipazione obbligatoria (totale): min-card (C1, A) > 0
        + max-card(C1, A): cardinalità massima di C1 in A
        + min-card(C, A) ≤ max-card(C, A)
        + **Valori utilizzati**
            - 0 = Cardinalità opzionale
            - 1 = Cardinalità obbligatoria
            - N
        + **Classificazione delle associazioni binarie**
            - Uno a uno
                * max-card(C1, A) = 1
                * max-card(C2, A) = 1
            - Uno a molti
                * max-card(C1, A) = 1
                * max-card(C2, A) = N
            - Molti a molti
                * max-card(C1, A) = N
                * max-card(C2, A) = N
        + **Numero massimo di corrispondenze**
            - A livello estensionale, se E(C1) ha m istanze e E(C2) ha n istanze:
                * max-card(C1, A) = N significa che il massimo numero di partecipazioni di un elemento di E(C1) in E(A) è limitato soltanto dal numero n di elementi in E(C2).
                * max-card(C2, A) = N significa che il massimo numero di partecipazioni di un elemento di E(C2) in E(A) è limitato soltanto dal numero di elementi m in C1.
                * Se si assume min-card(C1, A) = min-card(C2, A) = 0 allora il numero massimo delle corrispondenze che si possono avere in E(A) è:
                    + Uno a uno: 
                        - min(m, n)
                    + Uno a molti: 
                        - n se max-card(C1, A) = N, max-card(C2, A) = 1
                        - m se max-card(C1, A) = 1, max-card(C2, A) = N
                    + Molti a molti: 
                        - n x m
        + **Numero minimo di corrispondenze**
            - È immediato ricavare che per rispettare i vincoli deve valere:
                * E(C1) ≥ min-card(C2, A)
                * E(C2) ≥ min-card(C1, A)
- **Analisi**
    * **Orientata alle funzioni**
        + L’obiettivo è rappresentare un sistema come:
            - Una rete di processi
            - Un insieme di flussi informativi tra processi
        + Data Flow Diagram (DFD)

        ![alt text](./res/dfd.png "Data Flow Diagram.")
    * **Orientata agli oggetti**
        + L'enfasi è posta:
            - Sull'identificazione degli oggetti e sulla loro classificazione
            - Sulle interrelazioni tra oggetti
        + Nel tempo le proprietà strutturali degli oggetti osservati restano abbastanza stabili, mentre l'uso che degli oggetti si fa può mutare in modo sensibile.
        + Schema Entity/Relationship

        ![alt text](./res/er.png "Schema E/R.")
    * **Orientata agli stati (controllo)**
        + Diagramma stati-eventi

        ![alt text](./res/stati-eventi.png "Diagramma stati-eventi.")
- **Progettazione di un DB**
    * **Progettazione concettuale** (+ raccolta e analisi)

    ![alt text](./res/dfd-er-stm.png "Diagramma concettuale.")
    * **Progettazione logica**

    ![alt text](./res/logico.png "Diagramma logico.")
    * **Progettazione fisica**: scelta delle strutture di memorizzazione e degli indici.
        + Schema fisico:
            ```
            Esempio per la tabella FASCICOLI:
            Indice su FASCICOLI.CodFascicolo (chiaveprimaria) clustered
            Indice su FASCICOLI.Stato (chiavesecondaria) unclustered
            Indice su FASCICOLI.DataIscrizione (chiavesecondaria) unclustered
            ........
            ```
    * **Progettazione dell'applicazione**: realizzazione di moduli software per le varie funzioni e per la gestione dell'interfaccia utente.