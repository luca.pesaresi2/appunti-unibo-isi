# Reti 2

### Parte 1

- Canale = il mezzo di trasporto dei singoli flussi informativi fra punti (nodi) remoti nello spazio
    - Monodirezionale
    - Bidirezionale
        - Simmetrico: uguale capacità per ogni direzione
        - Asimmetrico: diversa capacità per ogni direzione
    - Punto-punto
    - Punto-multipunto 
        - Broadcast: un nodo trasmette allo stesso tempo a tutti i nodi della rete
        - Multicast: un nodo trasmette allo stesso tempo ad un sottoinsieme dei nodi
    
- La rete di telecomunicazioni deve permettere il riuso dei canali; siamo tantissimi, non possiamo avere un canale completamente dedicato che ci connette agli altri

- Componenti della rete:
    - Terminali
    - Mezzi trasmissivi
    - Nodi di commutazione: utilizzano i mezzi trasmissivi al fine di creare canali di comunicazione sulla base delle richieste degli utenti

- Topologie di rete
    - Maglia completa (resistente ma costosa)
        - Un collegamento per ogni coppia di nodi
        - N nodi implicano N(N-1)/2 collegamenti
    - A stella (non resistente ma cheap)
        - N collegamenti
        - Centro stella (attivo o passivo) deve smistare le informazioni
    - Anello
        - Monodirezionale (se un collegamento si interrompe la rete muore)
        - Bidirezionale (più resistente, più complessa)
    - Bus
        - Attivo o passivo (poco resistente ma cheap)
        - Bidirezionale (il mezzo di trasmissione è condiviso)
    - Gerarchica (organizzata su più livelli)
        - Terminali connessi ai nodi periferici (rete di accesso)
        - Nodi periferici connessi tramite nodi intermedi
        - Interconnessione a lunga distanza con nodi di transito tipicamente interconnessi a maglia completa (rete di transito)

- Tassonomia dei servizi
    - Interattivi: interazione fra sorgente e destinazione
    - Distributivi: la sorgente diffonde informazioni in modo indipendente ad un numero imprecisato di destinazioni
        - Senza controllo di presentazione: l’utente di destinazione non controlla l’ordine con cui ricevere le informazione
        - Con controllo di presentazione: l’utente di destinazione può controllare l’ordine con cui ricevere le informazione

- Servizi multimediali
    - Monomediale: trasporta informazioni di un solo tipo o più tipologie di informazione sotto forma di uno stesso segnale
    - Multimediale
        - Trasporta informazioni di almeno due tipologie diverse
        - Le diverse tipologie di informazioni sono trasportate dalla medesima rete ma con modalità distinte (protocolli diversi)
    
- Trasparenza
    - Semantica
        - Riguarda l’integrità delle informazioni trasportate
        - Richiede di attuare procedure di recupero da situazioni di errore che possono insorgere nella rete
        - Es: Applicazioni non real-time = bassa probabilità di errore
    - Temporale
        - Riguarda la variabilità dei ritardi di transito
        - Es: Applicazioni real-time = basso ritardo e jitter (se un messaggio arriva in un minuto e quello dopo in un minuto e dieci, quel dieci è il jitter)
            - I servizi che richiedono la trasparenza temporale per la corretta interpretazione dell’informazione sono detti isocroni
            - Es: Applicazione audio: al ricevitore si memorizzano i campioni in una memoria tampone (playback buffer) in modo da garantire la conversione con la corretta tempistica

- Servizio e canale
    - Non esiste diretta corrispondenza fra tipologia di canale e tipologia di servizio; ad esempio lo stesso servizio multicast può essere implementato con:
        - Canali broadcast
        - Canali punto-punto
        - Architettura mista

- Codifica
    - Di sorgente: da messaggio originale a messaggio adatto alla rete
        - Diminuisce la velocità di emissione senza comprometterne la fruibilità dell’informazione
    - Di canale: da messaggio di rete a messaggio adatto al canale
        - Aggiunge bit di ridondanza per il controllo dell’errore
    - Di linea: da messaggio di canale a messaggio adatto al mezzo trasmissivo

- Multiplazione
    - Più canali sono trasportati dallo stesso mezzo di trasmissione (collegamento)
    - Si può realizzare utilizzando
        - Tempo (TDM, Time Division Multiplexing, es: in questo tempo ti connetti tu, in quest'altro lui)
            - Slotted
                - L'asse dei tempi è suddiviso in intervalli di durata prefissata (slot)
                - Le unità informative hanno tutte la stessa lunghezza commisurata al singolo slot
                - Tipi
                    - Framed (S-TDM, Synchronous TDM)
                        - Gli slot vengono strutturati in trame (frame)
                        - Si sinconizza la trama
                    - Unframed (A-TDM, Asynchronous TDM)
                        - Gli slot si susseguono senza una struttura predefinita
                        - Occorre un sistema di sincronizzazione di slot
            - Unslotted
                - L’asse dei tempi non è suddiviso a priori
                - Si possono adottare unità informative di lunghezza variabile
                - E' necessario un sistema esplicito di delimitazione delle unità informative
        - Frequenza (FDM, Frequency Division Multiplexing, es: radio)
        - Codice (CDM, Code Division Multiplexing, es: PENSO che il terminale decodifichi il messaggio, così facendo trova fra le altre cose un codice e in base a quello decide se il messaggio è per lui o meno)
        - Spazio (es: prendo strade diverse)

- Assegnazione della banda
    - Statica
        - Flusso informativo = banda dedicata (bit/sec)
        - La banda non può cambiare a comunicazione in corso
        - La richiesta complessiva di banda è ben controllabile se si controlla il numero di flussi attivi
    - Dinamica
        - Molti flussi informativi condividono liberamente la banda in base alle necessità
        - La banda può cambiare a comunicazione in corso
        - La richiesta complessiva di banda può diventare intollerabile (congestione)

- ISO-OSI
    - Tutte  le reti di calcolatori della prima generazione  nascono e si evolvono come sistemi chiusi, ognuno definisce le impostazioni che preferisce, questo crea incompatibilità, ponendo ostacoli alla comunicazione
    - A partire dal 1976 la ISO ha dato il via a lavori per giungere ad una serie di standard unificati per la realizzazione di reti di calcolatori aperte basata sullo standard Open System Interconnection Reference Model (OSI-RM)
        - Architettura a strati
            - Vantaggi
                - Scompone il problema in sottoproblemi più semplici da trattare
                - Rende i vari livelli indipendenti
                - Refinendo solamente servizi e interfacce, livelli diversi possono essere sviluppati  da enti diversi
            - Modello

                ![alt text](./res/arc-strat.png "Architettura a strati")

                - 1, 2, 3: lower o network oriented layers, funzione di ripetizione (relay), operano link-by-link
                - 5, 6, 7: upper o application oriented layers
                - Lo strato 4 funge da raccordo fra gli upper e lower layers
                - Gli strati dal 4 in su operano solo end-to-end
            - Entità: ogni elemento attivo in uno strato, ce ne può essere anche più di una ed è identificata da un nome simbolico (title)
            - Protocollo: regole di dialogo fra entità dello stesso livello
            - Interfaccia: regole di dialogo fra entità di livelli adiacenti (non è necessario che sia standard, tanto è "interna")
            - Trasferimento dei dati
                - N-Protocol Data Unit (PDU): dati trasferiti fra entità di strato N
                - N-Service Data Unit (SDU): dati passati allo strato N dallo strato N+1
                    - E’possibile dividere il contenuto di una SDU in una o più PDU (segmentazione), la ricostruzione si dice riassemblamento
                - N-Service Access Point (SAP): indirizzo di identificazione del flusso dati fra N+1 ed N
                    - Un’entità di strato N può servire più (N)-SAP contemporaneamente
                    - Un utilizzatore di strato N può servirsi di più (N)-SAP contemporaneamente

                        ![alt text](./res/sap-good.png "Giusto uso dei SAP")

                    - Non è permesso connettere più (N)-user allo stesso (N)-SAP, si genererebbe ambiguità

                        ![alt text](./res/sap-bad.png "Errato uso dei SAP")

                - N-Protocol Control Information (PCI): informazioni aggiuntive per il controllo del dialogo a livello N
                
                ![alt text](./res/tras-dat.png "Schema incapsulamento e trasferimento dati")

                ![alt text](./res/flusso-info.png "Flusso delle informazioni")

            - Multiplazione: più connessioni di strato N vengono mappate in una di strato N-1
            - Splitting: più connessioni di strato N-1 vengono mappate in una di strato N
            - Strati (OSI)
                1. Fisico: specifica le modalità di invio dei singoli bit sul mezzo di fisico di trasmissione
                2. Linea: rende affidabile il collegamento fra i nodi di rete 
                3. Rete: fa giungere le unità di informazione, dette pacchetti (packets), al destinatario scegliendo la strada attraverso la rete 
                4. Trasporto: fornisce un canale sicuro end-to-end, svincolando gli strati superiori da tutti i problemi di rete
                5. Sessione: suddivide il dialogo fra le applicazioni in unità logiche(dette appunto sessioni)
                6. Presentazione: adatta il formato (sintassi) dei dati usato dagli interlocutori preservandone il significato (semantica)
                7. Applicazione: rappresenta il programma applicativo
            - Strati (TCP/IP)

                ![alt text](./res/osi-tcp-ip.png "OSI vs TCP/IP")
            
- Funzioni della rete
    - Trasmissione: trasferimento fisico del segnale
    - Commutazione: instradamento delle informazioni all’interno della rete
        - Di circuito: la rete crea un canale di comunicazione dedicato fra due terminali che vogliono colloquiare, finchè la connessione non è chiusa il canale rimane allocato
            - Fasi
                - Instaurazione circuito
                - Dialogo
                - Disconnessione circuito
        - Di pacchetto (tecnologia dominante): i messaggi vengono suddivisi in sotto-blocchi di lunghezza massima prefissata detti pacchetti che vengono trasmessi da un nodo di commutazione all'altro utilizzando in tempi diversi risorse comuni (net neutrality, nessuno ha priorità)
            - Tecniche
                - Circuito virtuale (connection oriented)
                - Datagramma (connectionless)
    - Segnalazione: scambio delle informazioni necessarie per la gestione della comunicazione e della rete stessa 
    - Gestione: tutto ciò che concerne il mantenimento delle funzioni della rete

### Parte 2

- Legge di Edholm: ogni circa 18 mesi la banda a disposizione dell’utente raddoppia, a costo circa costante

- Attenuazione
    - Qualunque mezzo trasmissivo  degrada il segnale elettromagnetico mentre questo si sposta, l'attenuazione è la misura della perdita di potenza del segnale (si misura in dB/km)
    - Linee in rame: $`A_{dB} = 10 \times log_{10} (\frac{P_{T}}{P_{R}}) = α \times \sqrt{f_{MHz}} \times L`$
        - Twisted Pair
            - Shielded (STP)
                - Nel cavo ogni coppia è avvolta in un conduttore che fa da schermo
                - Maggiore costo del cavo
                - Lo schermo deve essere messo a massa
            - Unshielded (UTP)
                - Meno costose e più semplici da posare

                ![alt text](./res/utp.png "UTP")

        - Cavo coassiale
            
            ![alt text](./res/cav-coa.png "Cavo coassiale")

    - Radio comunicazioni
        - Mezzo naturalmente broadcast
        - Limitazione: lo spettro è finito e può essere disturbato
        - Attenuazione
            - Cresce con la distanza con legge polinomiale
            - Cresce con il quadrato della frequenza
            - Le antenne diventano più efficienti quando la frequenza cresce
        - Le onde elettromagnetiche si propagano in linea retta
            - Sotto i 3 MHz: visibilità diretta o onda di terra
            - Fra 3 e 30 MHz: propagazione ionosferica
            - Sopra i 30 MHz: solo visibilità diretta (ponti radio)
        - Esempi
            - Diffusione radiofonica/televisiva

                ![alt text](./res/dif-rad-tel.png "Diffusione radiofonica/televisiva")

            - Sistemi radiomobili (cellulari)

                ![alt text](./res/radiomobili.png "Sistemi radiomobili")

- Fibra ottica: utilizzare la luce per trasportare l’informazione
    - Filamento di vetro o plastica molto sottile a densità differenziata grande più o meno come un capello

        ![alt text](./res/fibra-ottica.png "Schema struttura fibra ottica")

    - Si costruisce con appositi macchinari che "filano" la fibra
    - Tipi (attenuazione più elevata se core più grande)
        
        ![alt text](./res/tipi-fibre.png "Tipi fibre ottiche")

    - Sistema di trasmissione
        - I sistemi tradizionali in fibra utilizzano una tecnica detta On/Off Keying(OOK)
        - La sorgente di luce (laser, LED) genera impulsi luminosi
        - Un rivelatore (fotodiodo) riceve gli impulsi
        - La propagazione è praticamente priva di errore
    - Paradosso: costa poco ed è più veloce del rame
    - Problema: difficili da giuntare
        - Tipi giunti
            - Stabili
                - Per giuntare i cavi questi devono essere estremamente allineati (altrimenti la luce che passa attraverso i core subisce l'effetto "scalosso"); per fare ciò si utilizza la giuntatrice la quale aiuta nell'allineamento ed emette un flash potentissimo che "salda" i cavi
                    - E' una cosa fattibile a livello di rete di trasporto (poche giunture) ma per quanto riguarda la rete di accesso (tantissime giunture) è più oneroso
            - Temporanei
    - E' meno affetta da affievolimento rispetto al rame, la fibra può percorrere anche centinaia di chilometri senza essere "ripetuta", il rame invece al massimo decine
    - Amplificatore ottico
        - E' una porzione di filo in erbio drogato (metallo) che amplifica il segnale 
    - Offtopic: si preferisce passare dal mare rispetto che dalla terra perchè costa meno, nel mare non ci sono tutte le asperità della terraferma
    - WDM (Wavelength Division Multiplexing)
        - Flussi dati diversi vengono trasmessi su colori (lunghezze d’onda) diversi nella stessa fibra
        - Commutazione
            - Trasporto flussi informativi di diversi clienti su diversi colori e uso appunto il colore per distinguere il punto di partenza e quello di arrivo
            - Sono necessari apparati capaci di selezionare il colore della luce in modo comandato (MEMs Switch)
    - Classificazione in base alla localizzazione dell’interfaccia elettro/ottica (EOI)
        - Fiber To The Home (FTTH)
        - Fiber To The Building (FTTB)
        - Fiber To The Curb (FTTC) e Fiber To The Cab (FTTCab): fino al cabinotto
        - FiberTo The Exchange (FTTE): fino alla centrale
    - "Passività"
        - Per connettere le centrali alle case uso un sistema passivo per cui solo la centrale manda il segnale della fibra, il ricevente semplicemente lo rimbalza modificato (come se bloccasse la luce sorgente o la facesse andare ad intermittenza)

### Parte 3

- Sincronismo di cifra (inizio trasmissione): nelle trasmissioni numeriche per riconoscere i bit in ricezione occorre determinare gli istanti di campionamento per ricostruire il sincronismo di cifra (colui che riceve elementi li legge nel momento giusto, chi legge si sincronizza a chi scrive)
    - Macromodi
        1. Chiunque nel mondo è sincronizzato su un orologio comune, così tutti sanno quando il messaggio parte e quando arriva (si potrebbe usare il GPS ma è molto dificcile perchè esiste il ritardo di propagazione)
        2. Due endpoint si sincronizzano (sincronizzazione asservita)
            - Modalità
                - Il canale può essere tenuto sempre pieno di bit
                    - L’aggancio avviene in fase di inizializzazione e viene poi sempre mantenuto
                    - Il protocollo di linea deve garantire la presenza di segnale anche quando non ha dati da trasmettere
                - Il canale può avere momenti di vuoto di segnale (BEST)
                    - All’inizio di ogni nuova trasmissione  deve essere inserito un preambolo di sincronismo
    
- Sincronismo di trama: distinguere un messaggio dall'altro
    - Modalità
        - Protocolli asincroni a livello di trama: le trame possono iniziare e finire in ogni istante, informazioni aggiuntive vengono usate per riconoscere  correttamente  inizio e fine delle trame
        - Protocolli sincroni a livello di trama: le trame devono iniziare e terminare in istanti predefiniti

- Messaggio "totale": Preambolo di sincronismo + sincronismo di inizio trama + dati + sincronismo di fine trama
    - Ricorda
        - Se io uso 10 byte per i messaggi di sincronizzazione e mando un messaggio di 10 byte (sincronizzazione esclusa) ho un'efficienza di 10/20 = 0.5
        - Se io uso 10 byte per i messaggi di sincronizzazione e mando un messaggio di 100 byte (sincronizzazione esclusa) ho un'efficienza di 100/100 = 0.9

- Garantire l'affidabilità
    - Prima di consegnare i dati allo strato superiore si controllano
        - Errori di trasmissione
            - Codifica di canale con codici a rivelazione  di errore
            - Conferma  di ricezione e ritrasmissione
        - Sequenzialità dei dati
            - Numerazione delle unità informative
            - Conferma  di ricezione e ritrasmissione
        - Flusso dei dati
            - Finestra scorrevole
            - Conferma  dei dati

- Controllo di errore
    - Metodi
        - Codici a blocco (BEST): prendo un blocco di k bit e lo elaboro (funzione combinatoria, hash) per creare un codice di controllo di lunghezza r (bit di ridondanza); quindi spedisco un totale di n = k + r bit
            - Sono disponibili $`2^{n}`$ parole di codice per trasportare $`2^{k}`$ messaggi
                - $`2^{k}`$ sono parole di codice ammesse (valide)
                - $`2^{n} - 2^{k}`$ sono parole di codice non ammesse (invalide)
            - Modalità
                - Codici a rivelazione di errore (linea di trasporto)
                    - La ricezione di una parola di codice invalida indica la presenza di errori di trasmissione, non si può dire quali siano i bit errati  quindi per garantire la trasparenza semantica è necessaria  la ritrasmissione dei dati errati
                - Codici a correzione di errore (strato fisico)
                    - Una parola di codice invalida indica la presenza di errori di trasmissione e permette di individuare la parola di codice valida corrispondente (in binario se un bit è sbagliato so per forza qualìè quello giusto)
            - Pro e contro
                - Il codice per la rivelazione di errore costa 1/10 di quello per la correzione, (se 1Mbit velocità e ogni trama = 1000 bit allora correzione = 10 bit aggiuntivi per trama, rivelazione = 1 bit aggiuntivo per trama)
                - Casi
                    - Tasso di errore per bit del canale pari a $`10^{-6}`$ (1 errore ogni 1000 trame)
                        - Correzione: 10000 bit aggiuntivi
                        - Rivelazione: 1000 bit (uno per trama) + 1001 bit (trama reinviata) = 2001 bit aggiuntivi [WINNER]
                    - Tasso di errore per bit del canale pari a $`10^{-5}`$ (1 errore ogni 100 trame)
                        - Correzione: 10000 bit aggiuntivi
                        - Rivelazione: 1000 bit (uno per trama) + 10*1001 bit (trame reinviate) = 11010 bit aggiuntivi
                    - Tasso di errore per bit del canale pari a $`10^{-4}`$ (1 errore ogni 10 trame)
                        - Correzione: 10000 bit aggiuntivi [WINNER]
                        - Rivelazione: 1000 bit (uno per trama) + 111*1001 bit (trame reinviate) = 112111 bit aggiuntivi
                - Conclusione: rivelazione  se il canale è affidabile, correzione se il canale produce molti errori di trasmissione
            - Come funziona

                ![alt text](./res/cod-bloc-fun.png "Funzionamento dei codici di blocco")

            - Implementazioni
                - Bit di parità (usato per comunicazioni con messaggi brevi)
                    - Ripasso xor
                        - 1 ^ 0 = 1
                        - 0 ^ 0 = 0
                        - 1 ^ 1 = 0
                        - 0 ^ 1 = 1
                    - Dati k bit di informazione $`b_{0}, \space b_{1}, \space ... \space, \space b_{k-1}`$
                        - $`b_{k} = b_{0} \space xor \space b_{1} \space xor \space b_{2} \space xor \space ... \space xor \space b_{k-1}`$: parità pari
                        - $`b_{k} = NOT[b_{0} \space xor \space b_{1} \space xor \space b_{2} \space xor \space... \space xor \space b_{k-1}]`$: parità dispari

                        ![alt text](./res/parit.png "Bit di parità")

                    - Proprietà
                        - Rivela sempre un numero dispari di errori
                        - Fallisce con un numero pari di errori
                - Checksum (estensione di bit di parità)
                    - Funzionamento
                        - Creazione: Si applica su parole di 16 bit, indipendentemente dalla lunghezza complessiva del blocco dati; viene eseguita la somma a complemento a 1 su questi blocchi di 16 bit e il risultato (invertito) viene messo nel field di checksum
                            - Esempio somma complemento a 1
                                ```
                                    11110010  +
                                    11110100
                                    ---------
                                   111100110
                                           1 // Il riporto viene aggiunto al risultato
                                    ---------
                                    11100111
                                ```
                            - Proprietà complemento a 1
                                - Blocco dati fatto di byte A, B, C, D, E, F, G, ...
                                - Parole di 16 bit [A,B], [C,D], [E,F], [G,H]
                                - Proprietà commutativa e associativa
                                    - [A,B]+[C,D] = [C,D]+[A,B]
                                    - ([A,B]+[C,D])+[E,F] = [A,B]+([C,D]+[E,F])
                                - Indipendenza dall’ordine dei byte
                                    - [A,B]+[C,D] = [X,Y] allora [B,A]+[D,C] = [Y,X]
                                    - Questa proprietà è molto importante perché rende il calcolo indipendente dalla rappresentazione  del numero a livello di sistema hardware "big-endian"  o "little-endian"
                        - Controllo: viene rigenerato il checksum partendo dagli stessi blocchi di dati includendo però il field del checksum; se il risultato tutti bit a 1 il controllo ha successo
                - Codici polinomiali (non ci saranno esercizi su questo, ma sai come funziona)
                    - Trasmissione
                        1. Viene stabilito un polinomio di grado r noto a trasmettitore e ricevitore (polinomio generatore $`G_{r}(x)`$)
                        2. k bit (da trasmettere) vengono posti in corrispondenza con un polinomio di grado k-1 nella variabile binaria x: $`P_{k-1}(x) = b_{0} + b_{1}x + b_{2}x^{2} + ... + b_{k-1}x^{k-1}`$
                        3. Si calcola il polinomio $`T_{n-1}(x)`$ da trasmettere
                            - Si moltiplica il polinomio $`P_{k-1}(x)`$ per $`x^{r}`$ con r = bit a zero posti in coda
                            - Si esegue la divisione polinomiale fra $`P_{k-1}(x) x^{r}`$ e $`G_{r}(x)`$ ottenendo un quoziente ed un resto
                                - $`P_{k-1}(x) x^{r} = G_{r}(x) Q_{k-1}(x) \space xor \space R_{r-1}(x)`$
                            - Si invia solo lo xor fra $`P_{k-1}(x) x^{r}`$ e il resto ($`T_{n-1}(x) =  P_{k-1}(x) x^{r} \space xor \space R_{r-1}(x)`$) che sarà multiplo del polinomio generatore
                    - Ricezione
                        1. Polinomio ricevuto: $`T'_{n-1}(x)`$
                        2. Se ci sono errori esisterà un polinomio (detto di errore) per cui: $`T'_{n-1}(x) = T_{n-1}(x) + E(x)`$
                        3. Il ricevitore esegue la divisione: $`\frac{T'_{n-1}(x)}{G_{r}(x)} = \frac{(T_{n-1}(x) + E(x))}{G_{r}(x)} = \frac{T_{n-1}(x)}{G_{r}(x)}+ \frac{E(x)}{G_{r}(x)}`$ 
                            - Se $`E(x) != 0`$ e $`E(x)`$ non è divisibile per $`G_{r}(x)`$, allora il resto della divisione precedente risulta diverso da 0 e viene quindi rilevato l'errore
                            - In sostanza dobbiamo dividere $`T'_{n-1}(x)`$ per $`G(x)`$, se abbiamo un resto c'è un errore
                        - NB: $`G_{r}(x)`$ va scelto per minimizzare la probabilità di non rivelare un errore, si deve quindi evitare che $`Resto[\frac{E(x)}{G_{r}(x)}] = 0`$
                            - Polinomio generatore scelto: $`G_{16}(x)= x^{16} + x^{12} + x^{5} + 1`$
                - Authomatic Repeat Request: i protocolli ARQ vengono utilizzati nello strato di linea ed in quello di trasporto in sinergia con una codifica a rivelazione di errore per appunto gestire e risolvere gli errori
                    - Funzionamento (alto livello)
                        1. Alle PDU viene applicata una codifica di canale
                        2. Il ricevitore
                            - Verifica la correttezza delle PDU ricevute grazie a rivelazione di errore
                            - Ignora le PDU errate
                            - Può far partire le procedure di ritrasmissione
                        3. Il trasmettitore
                            - Ritrasmettele trame non correttamente ricevute
                            - Su indicazione del ricevitore o alla scadenza del time-out
                    - Numerazione
                        - Trasmettitore  e ricevitore  mantengono  due contatori:
                            - S conta in modo sequenziale le unità informative inviate (permette il "posizionamento" del flusso)
                            - R conta le unità informative ricevute in modo corretto (permette  di confermare  di ricezione)

                            ![alt text](./res/numeraz.png "Esempio di numerazione")
                    
                    - Conferma
                        - La corretta ricezione viene confermata dal ricevitore inviando al trasmettitore il proprio valore di R (le PDU ricevute in modo corretto fanno aumentare R, le altre no)
                        - Tipi
                            - Esplicita: ogni PDU ricevuta correttamente genera una conferma 
                            - Implicita (cumulativa): una PDU di conferma con R = n conferma la ricezione fino a n-1
                            - In piggybacking: viaggia inserita (a “cavalluccio”) in una PDU contenente dati utili
                        - ACK: PDU specializzate che non portano dati di utente ma solamente informazioni di controllo per il protocollo
                    - Finestra scorrevole (Codici di canale + Numerazione delle unità informative + Conferma di ricezione)
                        - Finestra di trasmissione
                            - W = numero massimo di trame che il trasmettitore può inviare senza ricevere alcuna conferma
                            - La numerazione delle trame viene effettuata modulo M con k bit -> M = $`2^{k}`$ (della serie le trame vanno da 0 a M-1)
                                - W <= M-1
                            - Si può procedere con la trasmissione di nuove trame solo al ricevimento delle conferme

                            ![alt text](./res/fin-tras.png "Esempio di finestra di trasmissione")

                        - Finestra di ricezione
                            - W = massimo numero di trame che il ricevitore può memorizzare prima di consegnare i dati allo strato superiore
                                - Go-back-n: W = 1
                                - Selective repeat: W > 1 
                            - La numerazione delle trame viene effettuata modulo M con k bit -> M = $`2^{k}`$ (della serie le trame vanno da 0 a M-1) e Wtrasmissione + Wricezione <= M (per evitare che si verifichi ambiguità di "tramsissione o ritrasmissione?")
                    - Controllo di flusso: accorda la velocità del  trasmettitore alla capacità del ricevitore (e della rete) in modo da non intasarli
                    - Recupero dell’errore
                        - Go-back-n 
                            - Viene persa la trama N
                            - Il ricevitore scarta tutte le trame successive a quella errata e, a seconda dell’implementazione:
                                - Segnala al trasmettitore la mancata ricezione della trama N

                                    ![alt text](./res/gbn2.png "Go back n con segnalazione")

                                - Rimane in silenzio senza inviare alcuna trama di segnalazione

                                    ![alt text](./res/gbn1.png "Go back n con timeout")

                            - Il trasmettitore ritrasmette tutte la trame a partire dalla numero N
                        - Selective repeat
                            - Viene persa la trame N
                            - Il ricevitore scarta la trama errata e segnalala la mancata ricezione della trama N
                            - Il trasmettitore ritrasmette solamente la trama N
                            - Il ricevitore riordinale trame nella memoria di ricezione

                            ![alt text](./res/selrep.png "Selective repeat")

                    - Round trip time (RTT):  tempo necessario per effettuare un’andata e ritorno sul canale
                    - Timeout: se le trame di conferma di ricezione sono perdute si potrebbe andare in stallo, per questo si utilizza il timeout
                        - Il timeout va relazionato al RTT
                            - Time out troppo breve = non si attende l’arrivo dell’ACK quindi si ha un invio non necessario  di trame duplicate
                            - Time out troppo lungo = inutile attesa prima di ritrasmettere  le trame errate
        - Codici convoluzionali: vengono calcolati r bit di ridondanza ogni k di informazione mediante reti logiche sequenziali; nel calcolo dei bit di ridondanza si tiene conto dei k bit di informazione e di variabili di stato dipendenti dalle operazioni passate

### [Parte 4 - Efficienza](./efficienza.md)

### Parte 5

- Enti che gestiscono gli standard di internet
    - Internet Engineering Task  Force (IETF): coordinare le attività di ingegnerizzazione ed implementazione
    - Internet Research Task  Force (IRTF): coordinare le attività di ricerca
    - IANA (Internet Assigned Number Authority): mantiene i database dei numeri che hanno significati convenzionali nei protocolli di Internet

- RFC (Request For Comment): documenti di pubblico dominio, i protocolli sono definiti in vari RFC

- Locator e Identifier
    - Identifier (URI, Uniform Resource Identifier)
        - NON E' UN IP

    - Locator (URL, Uniform Resource Locator): la risorsa R è univocamente identificata da un indirizzo che la localizza

        ![alt text](./res/url.png "Esempio di URL")

- Protocollo di trasporto
    - Si occupa del trasporto dei dati end-to-end
    - I flussi dati di diverse applicazioni sono distinguibili sulla base del numero di porta
        - Porta
            - 16 bit (0..65535)
            - Locale al singolo calcolatore, ripetute su tutti i calcolatori
            - Condiviso fra tutti i protocolli di trasporto
            - Regole
                - 1..1023: riservati, possono essere usati solo dai server
                    - "Well Known Ports" (assegnate dalla IANA)
                - 1024..49151: registrati, sono usati da alcuni servizi ma anche da client
                - 49151..65535: ad uso dei client
    - Protocolli
        - UDP (User Datagram Protocol)
        - TCP (Transmission Control Protocol)
        - RTP (Real Time Transmission Protocol)
    - Rimappatura strati
        - Strati 5..7 = Applicazione
        - Strati 1..2 = Link
        - Gli altri rimangono invariati

- Indirizzo IP
    - 32 bit
    - Esempio
        - Binario: `10001001.11001100.11010100.00000001`
        - Decimale: `137.204.212.1`
    - Identifica l'interfaccia con cui un calcolatore si connette ad internet (un calcolatore può avere anche più di una interfaccia quindi più di un indirizzo IP)
    - Progettato per funzionare a commutazione di pacchetto in modalità connectionless
    - Si prende carico della trasmissione di datagrammi da sorgente a destinazione, attraverso reti eterogenee
    - Identifica host e router tramite indirizzi di lunghezza fissa, raggruppandoli in reti IP
    - Frammenta e riassembla i datagrammi quando necessario
    - Offre un servizio di tipo best effort, cioè non sono previsti meccanismi per aumentare l’affidabilità del collegamento end-to-end ed eseguire il controllo di flusso e della sequenza

- LAN (Local Area Network)
    - Tipicamente un calcolatore si connette alla rete tramite una rete di accesso locale detta LAN
    - Canale di trasmissione/ricezione condiviso fra tutti i calcolatori della LAN
        - Come evitare di parlare uno sull’altro rendendo la comunicazione incomprensibile?
            - È necessario un indirizzo LAN (MAC Address); l’interfaccia di strato 2 (che parla e riceve con la LAN) legge e passa allo strato 3 solamente i dati di sua pertinenza
            - MAC Address
                - 48 bit
                - Scritti nella scheda di rete
                - Univoci
                - Modalità trasmissione
                    - Singolo destinatario (unicast): `00-60-b0-78-e8-fd`
                    - Indirizzo di gruppo (multicast): il primo bit deve essere a 1
                    - Tutte le stazioni (broadcast): `ff-ff-ff-ff-ff-ff`

- Esempio riassuntivo

    ![alt text](./res/escom.png "Esempio generico di un collegamento fra LAN")

    - Per identificare il singolo flusso è sufficiente conoscere:
        - IP sorgente
        - IP destinazione
        - Porta sorgente
        - Porta destinazione

- Metodologia di comunicazione
    - Client-server
        - Server: mettono a disposizione risorse di elaborazione e dati
            - Si predispone a ricevere una connessione eseguendo una apertura passiva creando una socket e mettendosi in ascolto in attesa dell’arrivo di una richiesta di connessione
        - Client: ospitano applicazioni che, al fine di svolgere le relative funzioni, si connettono ai server per ottenere risorse ed informazioni
            - Esegue una apertura attivatentando di collegarsi al processo server di destinazione
    - P2P
        - Gli host in rete sono tutti equivalenti (peer, appunto) e fungono alternativamente sia da client che da server verso altri nodi
        - E' una variante di client-server perchè c'è sempre la logica client-server sotto

- Architettura di rete

    ![alt text](./res/arcrete.png "Architettura di rete")

- Formato pacchetto IP

    ![alt text](./res/pacchettoip.png "Formato pacchetto IP")

    - Version: versione protocollo ip (4)
    - IHL (IP Header Length): lunghezza dell’header, espressa in parole di 32 bit (min = 5)
    - Type of service: indicazione sul tipo di servizio richiesto, usato anche come sorta di priorità all'interno della stessa comunicazione (mai usato)
    - Total  length: lunghezza totale del datagramma (in byte, max = 65535)
    - Identification: valore intero che identifica univocamente il datagramma; indica a quale datagramma appartenga un frammento (len = 16 bit)
    - Flag
        - Bit 0 sempre a 0
        - Bit 1: don’t fragment (DF)
            - 0: si può frammentare
            - 1: non si può frammentare
        - Bit 2: more fragments (MF)
            - 0: ultimo frammento
            - 1: frammento intermedio
    - Fragment offset: posizione di questo frammento nel datagramma, come distanza in unità di 64 bit dall’inizio
        - Il datagramma IP viene virtualmente suddiviso in sotto-blocchi di 8 byte (64 bit)
        - Il primo blocco del datagramma è il numero 0, i blocchi successivi sono logicamente numerati sequenzialmente
        - Il numero logico del primo blocco viene scritto nel Fragment offset di ogni datagramma
        - Perchè?
            - I pacchetti passano da tanti router diversi che utilizzano varie tecnologie diverse, ognuno ha una dimensione di pacchetto che può inviare; per questo i pacchetti vengono segmentati (anche più volte) e per questo è necessario il Fragment offset
        - Frammentazione: per segmentare, i router, replicano l'header del pacchetto in ogni datagram e splittano i dati del pacchetto nei vari datagram così creati
        - Riassemblamento: in base agli offset

        ![alt text](./res/frammentazione.png "Spiegazione grafica frammentazione")

        - Chi frammenta i datagrammi?
            - Qualunque apparato di rete dotato di protocollo IP può frammentare un datagramma
            - Tipicamente  i nodi intermedi non riassemblano, ma lo fa solamente il terminale ricevente
    - Time to live (TTL): max numero di nodi attraversabili
        - Il nodo sorgente attribuisce un valore maggiore di 0 a TTL
        - Ogni nodo che fa passare il datagramma pone TTL = TTL -1 
        - Il primo nodo che vede TTL = 0 distrugge il datagramma
    - Protocol: indica a quale protocollo di livello superiore appartengono i dati del datagramma
    - Header checksum: controllo di errore della sola intestazione, viene ricalcolato da ogni nodo
    - Source and Destination Address: indirizzi sorgente e destinazione
    - Options: contiene opzioni relative al trasferimento del datagramma, di lunghezza variabile
    - Padding: bit privi di significato aggiunti per fare in modo che l’intestazione sia con certezza multipla di 32 bit

- Instradamento
    - Network IP
        - E' una specie di isola
        - Contiene calcolatori che fungono da nodi terminali della rete detti host
            - Gli host sono connessi dalla medesima infrastruttura di rete fisica (livelli 1 e 2)
            - Gli host sono in grado di parlare tra loro grazie alla tecnologia con cui la network IP a cui appartengono è implementata
        - Le isole sono interconnesse da apparati che svolgono la funzione di “ponte” (router/gateway) che lavorano dal livello 1 al livello 3 OSI
            - Possono comunicare anche se utilizzano tecnologie diverse
            - Quando un host deve mandare un messaggio ad un altro host di un'altra network IP si crea un percorso che passa per n gateway (anche di altre reti) e arriva al destinatario (instradamento); il salto da un router all'altro viene detto hop
        - Rete logica: la network IP a cui un Host appartiene logicamente
        - Rete fisica: la rete (tipicamente LAN) a cui un Host è effettivamente connesso
    - Semantica indirizzo IP
        - Due parti
            - Network (Net) ID (sinistra): prefisso che identifica la Network  IP a cui appartiene l’indirizzo; tutti gli indirizzi di una medesima Network IP hanno il medesimo Network ID
            - Host ID (destra): identifica l’host (l’interfaccia) vero e proprio di una certa Network
        - IP riservati a reti private
            - da 10.0.0.0 a  10.255.255.255
            - da  172.16.0.0 a  172.31.255.255
            - da 192.168.0.0 a 192.168.255.255
    - Netmask: indirizzo che divide l'IP in Net ID e Host ID
        - I bit a 1 della netmask identificano i bit dell’indirizzo IP che fanno parte del Net ID
        - Notazioni
            - Dotted-decimal: `11111111.11111111.11111111.11000000 = 255.255.255.192`
            - Esadecimale: `11111111.11111111.11111111.11000000 = ff.ff.ff.c0`
            - Abbreviata: `11111111.11111111.11111111.11000000 = /26`
    - Tabella di instradamento
        - Righe (route): insieme di informazioni relative alla singola informazione di instradamento
        - Colonne (campi): informazioni del medesimo tipo relative a diverse opzioni di instradamento
            - Destinazione (D): numero IP destinazione
            - Netmask (N): maschera di rete, identifica il Net ID
            - Gateway (G): numero IP a cui consegnare il datagramma
            - Interfaccia di rete (IF): interfaccia di rete da utilizzare (loopback compreso) per la consegna del datagramma
            - Metrica (M): specifica il “costo” di quel particolare route (possono esistere più route verso una medesima destinazione)
        - Come funziona

            ![alt text](./res/routab.png "Esempio di tabella di routing")

            1. Il nodo riceve un datagramma e ne estra l'indirizzo IP di destinazione
            2. Table lookup: il nodo interrogherà la tabella dal basso verso l'alto (in generale dalla riga che presenta una netmask con un numero maggiore di bit a uno in su; questo perchè così si va dal "particolare" al "generale") cercando l'IP di destinazione che matcha servendosi anche della netmask
                - Longest prefix match: viene eseguito l'AND fra l'IP in input e la Netmask e, se il risultato matcha con l'IP di destinazione presente in tabella la route è quella giusta
            3. Se il route esiste il nodo esegue l’azione di instradamento suggerita dai campi G e IF, altrimenti genera un messaggio di errore (la riga con gli zeri mi darà sempre una risposta giusta)
        - Ruolo del gateway: è una sorta di portone di una determinata subnet, è lui che si preoccupa di ricevere il datagramma diretto ad un host della sua rete e reindirizzarlo all'host in questione (se la trasmissione è fra due host della stessa rete il gateway non è interrogato)
    - Tipi
        - Direct delivery: IP sorgente e IP destinatario sono sulla stessa rete fisica quindi l’host sorgente spedisce il datagramma direttamente al destinatario (Win: gateway = IP locale, Linux: gateway = 0.0.0.0)
        - Indirect delivery: IP sorgente e IP destinatario non sono sulla stessa rete fisica quindi l’host sorgente invia il datagramma ad un router intermedio che tramite il routing si preoccuperà di recapitare il messaggio al router appartenente alla rete dell'host destinazione, qusto router invierà poi il messaggio all'host in questione (gateway = IP router da contattare)

- Address Resolution Protocol (ARP)
    - Funzionamento
        1. Il nodo sorgente invia una trama broadcast (ARP request) contenente l’indirizzo IP del nodo destinazione; tutte le stazioni della rete locale leggono la trama broadcast
        2. Il destinatario risponde al mittente, inviando un messaggio (ARP reply) che contiene il proprio indirizzo fisico; così l'host sorgente è in grado di associare l’appropriato indirizzo fisico all’IP destinazione
        - Ogni host mantiene una tabella (cache ARP) con le corrispondenze fra indirizzi logici e fisici
    - Comando: `arp –a` visualizza il contenuto della cache ARP con le diverse corrispondenze tra indirizzi IP e MAC

- Internet Control Message Protocol (ICMP): protocollo di controllo del protocollo IP, segnala errori e malfunzionamenti
    - ICMP è incapsulato dentro IP
    - Pacchetto

        ![alt text](./res/pacchettoicmp.png "Struttura pacchetto ICMP")

        - Type: definisce il tipo
            - Di errore
            - Di richiesta di informazioni
        - Code: descrive il tipo di errore e ulteriori dettagli
        - Checksum: controlla i bit errati nel messaggio ICMP
        - Additional Fields: dipendono dal tipo di messaggio ICMP
        - Data: intestazione e parte dei dati del datagramma che ha generato l’errore
    - Tipi di errori
        - 3: Destination Unreachable
            - 0 = sottorete non raggiungibile
            - 1 = host non raggiungibile
            - 2 = protocollo non disponibile
            - 3 = porta non disponibile
            - 4 = frammentazione necessaria ma bit don’t fragment settato
        - 11: Time Exceeded
            - 0 = nel router quando il TTL di un  datagramma si azzera ed il datagramma viene distrutto
            - 1 = in host quando un timer si azzera in attesa dei frammenti 
        - 4: Source Quench
            - I datagrammi arrivano troppo velocemente
        - 5: Redirect
            - Generato da un router per indicare all’host sorgente un’altra strada più conveniente per raggiungere l’host destinazione
    - Informazioni
        - Ping
            - Messaggi
                - 8: Echo
                - 0: Echo Reply
            - Additional Fields
                - Identifier: identifica l’insieme degli echo appartenenti allo stesso test
                - Sequence Number: identifica ciascun echo nell’insieme
                - Optional Data: usato per inserire eventuali dati di verifica
        - Timestamp
            - Messaggi
                - 13: Timestamp Request
                - 14: Timestamp Reply
            - Utilità: serve per valutare il tempo di transito nella rete, al netto del tempo di processamento
        - Address mask
            - Messaggi
                - 17: Address Mask Request
                - 18: Address Mask Reply
            - Utilità: inviato dall’host sorgente all’indirizzo di broadcast per ottenere la subnet mask
        - ...
    - Comandi utili
        - `ping`: utilizza gli echo per fare delle misurazioni fra due host
        - `traceroute`: permette di conoscere il percorso seguito dai pacchetti inviati
            - SORG invia a DEST una serie di pacchetti ICMP di tipo ECHO con un TTL progressivo da 1 a 30
            - Ciascun nodo intermedio decrementa TTL
            - Il nodo che rileva TTL = 0 invia a SORG un pacchetto ICMP di tipo TIME EXCEEDED
            - SORG costruisce una lista dei nodi attraversati fino a DEST
    
- Dynamic Host Configuration Protocol (DHCP)
    - Configurazione automatica e dinamica (mediante server su porta 67 UDP) di
        - Indirizzo IP
        - Netmask
        - Broadcast
        - Host name
        - Default gateway
        - Server DNS
    - Funzionamento
        1. Quando un host attiva l’interfaccia di rete, invia in modalità broadcast un messaggio DHCPDISCOVER in cerca di un server DHCP
        2. Ciascun server DHCP presente risponde all’host con un messaggio DHCPOFFER con cui propone un indirizzo IP
        3. L’host accetta una delle offerte proposte dai server e manda un messaggio broadcast DHCPREQUEST in cui richiede la configurazione, specificando il server
        4. Il server DHCP risponde all’host con un messaggio DHCPACK specificando i parametri di configurazione

- Packet Filter: permette/blocca l’invio di pacchetti da/verso determinati indirizzi
    - Livelli   
        - IP

            ![alt text](./res/pacfil.png "Packet filter a livello IP")

        - Trasporto (rompo lo standard ISO/OSI)

            ![alt text](./res/pacfil1.png "Packet filter a livello di trasporto")

        - Applicazione (mediante un proxy detto application layer gateway)

            ![alt text](./res/pacfil2.png "Packet filter a livello applicazione")

    - Applicazione: firewall
        - Può essere
            - Software installato direttamente sull'host
            - Macchina dedicata
        - Politiche
            - Default deny: tutti servizi non esplicitamente permessi sono negati
            - Default permit: tutti i servizi non esplicitamente negati sono permessi
        - Implementazione
            - Packet filter
                - Si interpone un router fra la rete locale ed Internet
                - Sul router si configura un filtro sui datagrammi IP da trasferire attraverso le varie interfacce
                - Il filtro scarta i datagrammi sulla base di
                    - Indirizzo IP sorgente o destinazione
                    - Tipo di servizio a cui il datagramma è destinato (porta TCP/UDP)
                    - Interfaccia di provenienza o destinazione
            - Proxy server
                - Nella rete protetta l’accesso ad Internet è consentito solo ad alcuni host
                - Si interpone un server apposito detto proxy server per realizzare la comunicazione per tutti gli host, questo evita un flusso diretto di datagrammi fra Internet e le macchine della rete locale
                    - Application level: proxy server dedicato per ogni servizio che si vuole garantire
                    - Circuit level gateway: proxy server generico in grado di inoltrare le richieste relative a molti servizi

- NAT (Network Address Translation): tecnica per il filtraggio di pacchetti IP con sostituzione degli indirizzi
    - Benefici
        - Efficiente uso della spazio degli indirizzi (uso di indirizzi privati nella LAN locale)
        - Rendere gli host interni non accessibili dall’esterno
        - Nascondere gli indirizzi e la struttura della rete
        - Include un packet filter, stateful packet inspection configurati dinamicamente
    - Funzionamento

        ![alt text](./res/nat1.png "Funzionamento NAT")

        ![alt text](./res/nat2.png "Funzionamento NAT")

- Classless VS Classfull: la logica degli indirizzi IP
    - Passato
        - Classi di network differenziate per dimensione
            - La parte iniziale del Net-ID differenzia le classi
                - `0`: classe A
                - `10`: classe B
                - `110`: classe C

            ![alt text](./res/oldip.png "Structure of old IP addresses")

            - Indirizzi riservati
                - `0.0.0.0`: indica l’host corrente senza specificarne  l’indirizzo
                - `Host-ID tutto a 0`: viene usato per indicare la rete
                - `Host-ID tutto a 1`: è l’indirizzo  di broadcast per quella rete
                - `0.x.y.z`: indica un certo Host-ID sulla rete corrente senza specificare il Net-ID
                - `255.255.255.255`: indirizzo  di broadcast su Internet
                - `127.x.y.z`: loopback, che redirige i datagrammi agli strati superiori dell’host corrente
        - Subnetting: n bit del Host-ID diventano parte del Net-ID
            - Si frammenta l’Host-ID in due parti:
                - La prima identifica la sottorete (subnet-ID)
                - La seconda identifica i singoli host della sottorete

                ![alt text](./res/subnetting.png "Struttura di un IP con subnetting")

            - Esempio: UNIBO
                - Network di classe B (137.204.0.0)
                - Dalla network di classe B si ricavano 254 network della dimensione di una classe C utilizzando la netmask `255.255.255.0`
            - NB: subnet diverse sono di fatto network diverse e quindi non comunicano, per farlo è necessario un gateway
                - Come fa un host ad accorgersi che non deve passare dal gateway per parlare con un altro host della sua sottorete? Tramite la subnet mask; se l'host fa parte della mia rete ha la mia stessa subnet mask
        - Supernetting: n bit del Net-ID diventano parte dell’Host-ID
    - Presente: CIDR (Classless InterDomain Routing)
        - Cambiamenti
            - Si decide di rompere la logica delle classi nei router
            - La dimensione del Net-ID può essere qualunque
            - Le tabelle di routing devono comprendere anche le Netmask
            - Reti IP definite da Net-ID/Netmask
        - Benefici
            - Allocazione di reti IP di dimensioni variabili e utilizzo più efficiente dello spazio degli indirizzi
            - Accorpamento delle informazioni di routing (più reti contigue rappresentate da un’unica riga nelle tabelle di routing)

- Pianificare la numerazione di reti IP
    - Esempio
        - Problema
            - Un’azienda possiede tre siti distribuiti su una grande area urbana: S1, S2, S3
            - Ciascun sito aziendale è dotato di infrastrutture informatiche comprendenti, tra l'altro, una LAN ed un router di uscita verso il mondo esterno; tutti i siti devono essere interconnessi tra loro con una rete a maglia completa
            - I siti sono così divisi:
                - S1, S2: 50 host
                - S3: 20 host
            - Si richiede di progettare una rete di classe C a cui viene assegnato l’indirizzo `196.200.96.0/24` comprensiva della numerazione dei router, definendo le relative netmask
        - Risoluzione
            - Schema

                ![alt text](./res/schemaesreti.png "Schema esercizio")

            - Scelta netmask

                ![alt text](./res/scnet.png "Scelta netmask esercizio")

            - Soluzione

                ![alt text](./res/soles.png "Soluzione esercizio")

### Parte 6

- Metodo base di funzionamento della rete: store and forward
    1. Il pacchetto entrante è verificato e memorizzato
    2. Si estraggono le informazioni di instradamento dall’intestazione (indirizzo, priorità, classe di servizio)
    3. Si confrontano queste informazioni con la tabella di instradamento identificando una o più uscite su cui inviare il pacchetto
    4. Il pacchetto è inserito nella coda relativa all’uscita prescelta, in attesa della effettiva trasmissione

- Instradamento (routing): decidere che percorso un datagramma deve seguire per raggiungere la destinazione dalla sorgente
    - Flooding (elaborazione nulla)
        - Funzionamento
            - I nodi ritrasmettono su tutte le porte di uscita ogni pacchetto ricevuto
            - Ogni pacchetto verrà ricevuto da tutti i nodi a cui è stato inviato, anche da quello a cui è stato destinato
            - Il primo pacchetto che arriva a destinazione ha fatto la strada più breve
        - Accortezze
            - Problema: proliferazione dei pacchetti
            - Soluzioni
                - Un nodo non ritrasmette il pacchetto nella direzione dalla quale è giunto
                - Ad ogni pacchetto viene associato un identificativo unico (l’indirizzo della sorgente e un numero di sequenza) e ciascun nodo mantiene in memoria una lista con gli identificativi dei pacchetti già trasmessiIl nodo crea una lista dei pacchetti ricevuti e ritrasmessi
                - Contatore del tempo di vita (TTL) di un pacchetto per evitare che giri all’infinito
    - Deflection routing (hot potato)
        - Funzionamento
            - Quando un nodo riceve un pacchetto lo ritrasmette sulla linea d’uscita avente il minor numero di pacchetti in attesa di essere trasmessi
        - Problemi
            - I pacchetti possono essere ricevuti fuori sequenza
            - Alcuni pacchetti potrebbero percorrere all’infinito un certo ciclo in seno alla rete, semplicemente perché le sue linee sono poco utilizzate 
            - Si deve prevedere un meccanismo per limitare il tempo di vita dei pacchetti
            - Non tiene conto della destinazione finale del pacchetto
    - Shortest path routing
        - Funzionamento
            - Si assume che ad ogni collegamento della rete possa essere attribuita una lunghezza (peso) 
            - L’algoritmo cerca la strada di lunghezza minima fra ogni mittente e ogni destinatario
            - Si applicano algoritmi di calcolo dello shortestpath(Bellman-Ford e Dijkstra)
            - L’implementazione può avvenire in modalità
                - Centralizzata: un solo nodo esegue i calcoli per tutti
                - Distribuita: ogni nodo esegue i calcoli per se
                    - Sincrona: tutti i nodi eseguono gli stessi passi dell’algoritmo nello stesso istante
                    - Asincrona: i nodi eseguono lo stesso passo dell’algoritmo in momenti diversi

- Rappresentazione della rete come grafo (se necessario modulo 4 da 10 a 31 ripasso grafi)
    - I nodi rappresentano i terminali ed i commutatori
    - Gli archi rappresentano i collegamenti
    - L’orientazione degli archi rappresenta la direzione di trasmissione
    - Il peso degli archi rappresenta il costo dei collegamenti

- Generazione della routing table
    - Routing distance vector
        - Il Routing Distance Vector nella rete Internet utilizza l’algoritmo di Bellman-Ford in una versione (di Ford-Fulkerson)
            - Distribuita: i nodi eseguono indipendentemente  i calcoli degli Shortest Path
            - Asincrona: l’esecuzione dei calcoli nei vari nodi non è sincronizzata
        - Funzionamento
            - Ogni nodo scopre i suoi vicini e ne calcola la distanza da se stesso
            - Ad ogni passo, ogni nodo invia ai propri vicini un vettore contenente la stima della sua distanza da tutti gli altri nodi della rete (quelli di cui è a conoscenza)
            - Problemi
                - Convergenza lenta e cold start: l’algoritmo converge al più dopo un numero di passi pari al numero di nodi della rete quindi se la rete è molto grande il tempo di convergenza può essere lungo; cosa succede se lo stato della rete cambia in un tempo inferiore a quello di convergenza dell’algoritmo? Si ritarda ulteriormente la convergenza
                - Bouncing effect
                    - Il link fra due nodi A e B cade
                        - A e B si accorgono che il collegamento non funziona e immediatamente pongono ad infinito la sua lunghezza
                        - Se altri nodi hanno nel frattempo inviato anche i loro vettori delle distanze, si possono creare delle incongruenze temporanee, di durata dipendente dalla complessità della rete (in esempio A crede di poter raggiungere B tramite un altro nodo C che a sua volta passa attraverso A)
                        - Queste incongruenze possono dare luogo a cicli, per cui due o più nodi si scambiano datagrammi fino a che non si esaurisce il TTL o finché non si converge nuovamente

                        ![alt text](./res/bouncing.png "Effetto bouncing")

                - Count to infinity

                    ![alt text](./res/counttoinf.png "Esempio di count to infinity")

                    - Situazione iniziale: D(AB) = 1, D(BC) = 1 e D(AC) = 2
                        - Link BC va fuori servizio 
                        - B riceve il DV di A che contiene l’informazione D(AC) = 2, per cui esso computa una nuova D'(BC) = D(BA) + D(AC) = 3
                        - B comunica ad A la sua nuova distanza da C
                        - A calcola la nuova distanza D(AC) = D(AB) + D'(BC) = 4
                        - La cosa può andare avanti all’infinito
                    - Soluzioni
                        - Quando una distanza assume un valore D(IJ) > D(max) allora si suppone che il nodo destinazione J non sia più raggiungibile
                        - Split horizon: se A instrada i pacchetti verso una destinazione X tramite B, non ha senso per B cercare di raggiungere X tramite A; di conseguenza non ha senso che A renda nota a B la sua distanza da X
                        - Triggered update: un nodo deve inviare immediatamente le informazioni a tutti i vicini qualora si verifichi una modifica della propria tabella di instradamento
                    - Ma..
                        
                        ![alt text](./res/problemagrande.png "Esempio di problema irrisolto")

                        - Inizialmente, A e B raggiungono D tramite C
                        - Dopo il guasto, C mette a ∞ la sua distanza da Dù
                        - Dopo aver ricevuto il DV da C, A crede di poter raggiungere comunque D tramite B
                        - Idem per B che crede di poter usare A
                        - Stavolta A e B trasmettono i propri DV a C
                        - Si crea di nuovo un loop e un problema di convergenza


            - Esempio

                ![alt text](./res/esrout.png "Esempio di discovery dei nodi vicini eccetera")

                - Distance Vector iniziali: DV(i)= {(i,0)}, per i = A,B,C,D,E
                - Distance Vector dopo la scoperta dei vicini
                    - DV(A) = {(A,0), (B,1), (C,6)}
                    - DV(B) = {(A,1), (B,0), (C,2), (D,1)}
                    - DV(C) = {(A,6), (B,2), (C,0), (D,3), (E,7)}
                    - DV(D) = {(B,1), (C,3), (D,0), (E,2)}
                    - DV(E) = {(C,7), (D,2), (E,0)}
                - Evoluzione tabelle di routing

                    ![alt text](./res/routtabes.png "Esempio di aggiornamento di tabelle di routing in una rete")

    - Routing link state
        - Raccolta informazioni
            - Ogni router deve
                - Comunicare con i propri vicini ed “imparare” i loro indirizzi (Hello Packet)
                - Misurare la distanza dai vicini (Echo Packet)
                - Costruire un pacchetto con lo stato delle linee (Link State Packet, LSP) che contiene la lista dei suoi vicini e le lunghezze dei collegamenti per raggiungerli
        - Diffusione ed elaborazione delle informazioni
            - I pacchetti LSP devono essere trasmessi da tutti i router a tutti gli altri router della rete tramite Flooding, per questo nel pacchetto LSP occorre aggiungere
                - Indirizzo del mittente
                - Numero di sequenza
                - Indicazione dell’età del pacchetto
            - Avendo ricevuto LSP da tutti i router, ogni router è in grado di costruirsi un’immagine della rete (si usa l’algoritmo di Dijkstra per calcolare i cammini minimi verso ogni altro router)
        - Esempio: determinare il percorso di lunghezza minima dal nodo a verso tutti gli altri
            - Legenda
                - Nelle righe grigio chiare della tabella indichiamo le distanze determinate a quel passo dell’algoritmo
                - Nelle righe grigio scure indichiamo la distanza che viene ritenuta la migliore determinando il nodo a cui fare riferimento per i calcoli al prossimo passo dell’algoritmo
            - Risoluzione

                ![alt text](./res/rolinstasche.png "Esempio routing link state")

                ![alt text](./res/esroutlin.png "Esempio routing link state")

- Router: nodo di commutazione nelle reti IP
    - Classificazione
        - SOHO (Small Office and HOme)
            - Utilizzo domestico o piccoli uffici
            - Interfaccia sulla LAN (switch con poche porte Fast Ethernet 100 Mbit/s e wi-fi)
            - Non eseguono i protocolli di routing
        - Router di accesso
            - Usati dagli ISP per fornire il servizio
            - Gran numero di medium-low speed ports (50 kbps ÷ 10 Mbps)
            - Vari protocolli e tecnologie di accesso
        - Enterprise/campus router
            - Interconnessione fra LAN per organizzazioni di medie dimensioni
            - Poche porte ad elevata velocità (Fast o Gigabit Ethernet)
        - Backbone router
            - Per reti di trasporto e connessioni inter-domain
            - Piccolo numero di porte ad elevata velocità (>= 1 Gbps)
            - Sistemi di garanzia dell’affidabilità (ridondanza, monitoraggio remoto...)
    - Funzioni
        - Routing
            - Scambio di informazioni con altri router 
            - Elaborazione locale (routing algorithm)
            - Popolazione delle tabelle di routing
        - Forwarding
            - Table lookup
            - Header update
        - Switching: trasferimento del datagramma da interfaccia di input a interfaccia di output
        - Trasmissione: trasmissione del datagramma sul mezzo fisico (utilizzando l’interfaccia di rete di output)
    - Routing vs Forwarding
        - Routing table (RIB, Routing Information Base)
            - Risultato dei protocolli e degli algoritmi di routing
            - Ogni entry include prefisso di routing, next hop e "metrica"
        - Forwarding table (FIB, Forward Information Base)
            - Costruita in base alla RIB tramite opportuni algoritmi, sceglie il percorso migliore
            - Ogni entry include anche l'output interface
            - Usata per il forward dei datagrammi
            - Ottimizzata per un table lookup più veloce

### Parte 7

- Routing gerarchico
    - Le aree di routing sono chiamate Autonomous System (AS, insieme di prefissi di rete IP gestiti in modo unitario da vari gestori)
    - Un AS può essere ulteriormente suddiviso in porzioni dette Routing Area (RA) interconnesse da un backbone (dorsale)
    - Ogni network IP è tutta contenuta in un AS o in una RA
    - Gli AS decidono autonomamente i protocolli (detti Interior Gateway  Protocol, IGP) e le politiche di routing (RIP, Routing Information Protocol e OSPF, Open Shortest Path First) che intendono adottare al loro interno (che possono essere varie)
    - I vari enti di gestione si devono accordare su quali protocolli (detti Exterior Gateway Protocol, EGP) utilizzare per il dialogo tra i router che interconnettono AS diversi
    - Esempi
        - UNIBO
            - Università di Bologna -> 137.204.0.0/16
            - Politecnico di Torino -> 130.192.0.0/16
            - Entrambi sono connessi al GARR, la rete italiana degli enti di ricerca, e tramite essa comunicano con il resto del mondo
        - Esempio di connessione fra AS

            ![alt text](./res/interas.png "Esempio di interconnessione fra AS")

    - ISP e Internet region
        - Internet Service Provider (ISP): organizzazione che fornisce servizi per l’utilizzo di Internet, solitamente si registra come AS
        - Internet region: porzione di Internet contenuta in una specifica area geografica, solitamente una nazione/un'insieme di nazioni
        - Un’Internet region è solitamente servita da più ISP 
        - Uno stesso ISP può servire più Internet Region
        - Classificazione ISP
            - Tier 1: ISP che all’interno di una “Internet Region” raggiunge tutte le reti senza accedere a servizi a pagamento di altri (in Italia Telecom Italia Sparkle)
                - Nazionali: servono una sola Internet region
                - Globali: hanno punti di accesso in paesi e continenti diversi
            - Tier 2: ISP che raggiunge l’Internet globale acquistando servizi di interconnessione da un Tier 1; può avere interconnessioni anche con più di un ISP Tier 1 nella stesse o in diverse Internet Region
            - Tier 3: ISP che per raggiungere l’Internet globale acquista servizi di interconnessione da un ISP Tier 2; può avere interconnessioni dirette (peering) con altri ISP Tier 3 che servono la stessa zona o zone limitrofe (con zona si intende al massimo una regione)

            ![alt text](./res/clasisp.png "Classificazione di ISPs")

    - Peering: interconnessione fra due AS stabilita al fine di scambiarsi traffico
        - Non ha carattere economico
        - Avviene fra ISP del medesimo livello
        - Policy
            - Ristretta: richiesta e approvazione
            - Aperta: approvazione immediata

        ![alt text](./res/peering.png "Peering")

    - ISP locali e POP
        - Un ISP locale fornisce il servizio a gruppi di utenti co-localizzati (singola città, area industriale ecc.)
        - Realizza un’infrastruttura con router e switch in un punto della zona detto Point of Presence (POP)
        - Indirizzamento
            - Un ISP dispone di un sottoinsieme di numeri IP da utilizzare per i suoi clienti
                - Se sono consecutivi possono avere lo stesso prefisso quindi unico Network ID
                - Se non sono consecutivi deve gestire più prefissi quindi più Network ID
        - Interconnessione
            - ISP che coprono la medesima zona geografica scambiano traffico mediante l'interconnessione di uno o pochi POP 

                ![alt text](./res/popconn.png "Connessione di uno o più POP")

            - Per connettere tutti gli AS non si creano connessioni da ognuno verso tutti gli altri, si adotta invece un metodo per cui alcuni ISP svolgono la funzione di AS di transito per interconnettere con una topologia “a stella” gli ISP (gli ISP specializzati nel fornire servizi di transito sono anche detti Network Service provider, NSP, e spesso coincidono con ISP di tier 1)
                - Internet Exchange Point (IX o IXP)
                    - Infrastrutture attraverso le quali gli ISP possono stabilire relazioni di peering
                    - L’IXP è costruito per permettere l’interconnessione diretta degli AS senza utilizzare reti di terze parti
    - Interior Gateway Protocol (IGP)
        - Routing Information Protocol (RIP)
            - Protocollo distance vector
            - Utilizza due tipi di messaggi
                - REQUEST: serve per chiedere esplicitamente informazioni ai nodi vicini
                - RESPONSE: serve in generale per inviare informazioni di routing (cioè i distance vector)
                    - Inviato:
                        - Periodicamente (ogni 30 secondi, con uno scarto di 1-5 secondi)
                        - Come risposta ad una richiesta esplicita
                        - Quando un'informazione di routing cambia (triggered update)
                    - Contiene il distance vector del router che lo invia
            - I messaggi RIP sono trasportati da UDP ed usano la porta 520 sia in trasmissione che in ricezione
            - Struttura messaggio
                - Parole di 32 bit, lunghezza massima di 512 byte
                - Campi
                    - command: distingue tra REQUEST (1) e RESPONSE(2)
                    - version
                    - address family identifier: indica il tipo di indirizzo di rete utilizzato, vale 2 per IP
                    - address: identifica la destinazione per la quale viene data la distanza
                    - metrica: è la distanza dalla destinazione indicata
            - Tabella di routing
                - Ogni riga contiene
                    - Indirizzo di destinazione
                    - Distanza dalla destinazione (metrica): hop-count (ogni link ha peso = 1), la distanza massima (∞) per è pari a 16
                    - Next-hop sul percorso verso la destinazione
                    - Timeout: se una route non viene aggiornata dopo TO = 180 secondi, la sua distanza è posta all’infinito (si ipotizza una perdita di connettività)
                    - Garbage-collection timer: dopo ulteriori GC = 120 secondi la route viene eliminata del tutto dalla tabella
                - Aggiornamento
                    - A riceve un RESPONSE da B
                        - Si controlla la correttezza dei dati
                        - Si considerano solo le voci icon distanze d(i) < ∞
                        - Si calcola d(i) = d(i) + 1
                    - Esiste già una entry per la destinazione i?
                        - NO
                            - Si crea una nuova entry
                                - La distanza è d(i)
                                - Il next-hop è B (mittente del RESPONSE)
                                - Si fa partire il timeout
                        - SI
                            - d(i) è minore di quella presente in tabella
                                - La entry viene aggiornata con next hop = B e distanza = d(i)
                                - Si fa ripartire il timeout
            - Problemi
                - Insicuro: chiunque trasmetta datagrammi dalla porta UDP 520 viene considerato come un router autorizzato
                - Non supporta CIDR
                    - `10.1.1.0-------routerA----200.200.200.0----routerB---100.100.100.0---routerC---10.2.2.0`
                        - RouterB riceve distance vector contenenti la rete 10.2.2.0 da RouterC e la rete 10.1.1.0 da RouterA
                        - In assenza di CIDR 10.1.1.0 e 10.2.2.0 sono indirizzi appartenti alla stessa rete di classe A 10.0.0.0/8 ma per RouterB 10.0.0.0/8 deve essere un'unica destinazione; RouterB è confuso
                - Fa uso di split horizon: RESPONSE di interfacce diverse possono essere diverse
                - Fa uso di triggered update: non è necessario indicare nella RESPONSE tutte le entry della tabella ma solamente quelle appena modificate
        - Routing Information Protocol v2 (RIPv2)
            - Retrocompatibile: campi in più ignorati da RIPv1
            - Supportato il CIDR (campo subnet mask)
            - Possibilità di autenticare chi invia i messaggi
            - Possibilità di indicare il proprio AS e di scambiare informazioni con protocolli EGP (campi route tag e routing domain)
            - Possibilità di specificare un next hop più appropriato
            - Comunque ha problemi
        - Open Shortest Path First (OSPF) [BEST]
            - Protocollo di tipo link state: invio di Link State Advertisement (LSA) a tutti gli altri router
            - Incapsulato in IP
            - Bilanciamento del carico: se un router ha più percorsi di uguale lunghezza verso una certa destinazione, il carico viene ripartito equamente su di essi
            - Autenticazione
            - Routing dipendente dal grado di servizio: i router scelgono il percorso sul quale instradare un pacchetto sulla base dell’indirizzo e del campo Type of Service dell’intestazione IP, tenendo conto che percorsi diversi possono offrire diversi gradi di servizio
            - Aree di routing
                - Un AS può essere suddiviso in porzioni dette Routing Area (RA) interconnesse da un backbone (Area 0)
                    - Ciascuna area risulta separata dalle altre per quanto riguarda lo scambio delle informazioni di routing e si comporta come un’entità indipendente
                    - Per interconnettere le aree vi devono essere router connessi a più aree e/o al backbone (almeno un router per area)
                - Classificazione dei router
                    - Internal Router: interni a ciascuna area
                    - Area Border Router: scambiano informazioni con altre aree
                    - Backbone Router: si interfacciano con il backbone
                    - AS Boundary Router: scambiano informazioni con altri AS usando un protocollo EGP
                - Tipi di route
                    - Intra-area: aggiornamento delle informazioni di routing pertinenti all'area
                    - Inter-area: Aggiornamento delle informazioni di routing pertinenti ad aree diverse da quella considerata
                    - Esterni: aggiornamenti delle informazioni di route provenienti da altri protocolli al di fuori del dominio OSPF

                ![alt text](./res/ospfrout.png "Aree di routing e tipologie di router OSPF")

            - Tipologie di rete
                - Point-to-Point
                - Broadcast Multi-Access: tutti gli N router connessi alla rete sono di fatto connessi con tutti gli altri

                    ![alt text](./res/bma.png "Rete BMA")

                - Non-Broadcast Multi-Access
            - Prossimità fra router
                - Tipi
                    - Vicini: due router che sono connessi alla medesima rete e possono comunicare direttamente

                        ![alt text](./res/nero.png "Router vicini")

                    - Adiacenti: due router che si scambiano informazioni di routing

                        ![alt text](./res/adjro.png "Router adiacenti")

                - In una rete ad accesso multiplo risulta molto più efficiente eleggere un Designated Router (DR) fra gli N vicini
                    - Ogni router della LAN è adiacente solo al DR
                    - Lo scambio di informazioni di routing avviene solo tra router adiacenti (cioè DR fa da tramite)
                    - Il DR è l’unico a comunicare la raggiungibilità di router e host della LAN al mondo esterno
                    - Occorre avere anche un Backup Designated Router (BDR) adiacente a tutti i router locali
            - Identificazione di router e priorità
                - Ogni router deve avere un identificativo univoco (router ID)
                    - Default: si prende l’indirizzo IP più alto fra quelli assegnati alle interfacce del router
                    - Si può assegnare manualmente un router ID ad ogni router (safe)
                - Ai singoli router di un’area possono essere associate delle priorità (0-255, di default 0) che verranno utilizzate nell’elezione del DR
                    - Elezione DR e BDR
                        - Ciascun router nella rete ad accesso multiplo
                            - Esamina la lista dei suoi vicini
                            - Elimina dalla lista tutti i router non eleggibili (ad esempio tutti quelli che hanno priorità nulla)
                            - Fra quelli rimasti seleziona il router avente la priorità maggiore (più alto router ID in caso di uguale priorità), questo sarà il DR
                            - Procedimento analogo per il BDR escludendo il DR appena scelto 
            - Link State Database: grafo orientato della rete sul quale ciascun router calcola lo shortest path tree (presente in ogni router)
            - Protocolli: basati su IP
                - Pacchetto
                    - Version
                    - Type
                        - 1: hello
                        - 2: exchange
                        - 3: link state request (exchange)
                        - 4: link state update (flooding)
                        - 5: link state acknowledge (flooding)
                    - Packet Length
                    - Router ID: indirizzo IP che identifica il router mittente
                    - Area ID: area di appartenenza
                    - Checksum: calcolato su tutto il pacchetto OSPF escludendo gli 8 byte del campo authentication
                    - AuType: tipo di autenticazione
                        - 0: nessuna
                        - 1: password nel campo authentication
                        - 2: autenticazione crittografica nel campo authentication
                - Tipi
                    - Hello
                        - Scopi
                            - Controllare l’operatività dei link
                            - Scoprire e mantenere relazioni fra vicini
                            - Eleggere DR e BDR
                        - Funzionamento
                            - I pacchetti HELLO sono inviati sulle interfacce periodicamente secondo quanto specificato dal parametro HelloInterval per scoprire i propri vicini
                            - Includono una lista di tutti i vicini (Neighbor) dai quali è stato ricevuto un pacchetto HELLO recente (cioè non più vecchio di RouterDeadInterval), si riesce così a conoscere se per ciascun vicino è presente un collegamento bidirezionale e se esso è ancora attivo
                            - Router Priority, Designated Router e Backup Designated Routersono utilizzati per l’elezione di DR e BDR
                            - Network Mask: indica la maschera relativa all’interfaccia del router (l’indirizzo è nell’header IP)
                    - Exchange
                        - Scopi
                            - Sincronizzare i link state database dei router adiacenti
                        - Funzionamento
                            - Si stabilisce chi è il master e chi lo slave
                            - Il master invia una serie di pacchetti Database Description(Type = 2) contenenti l’elenco dei LSA del proprio database
                            - Lo slave risponde con l’elenco dei LSA del suo database
                            - Ciascuno dei due router confronta le informazioni ottenute con quelle in proprio possesso; se nel proprio database ci sono dei LSA meno recenti rispetto all’altro, questi vengono richiesti con un successivo pacchetto Link State Request(Type = 3)
                    - Flooding
                        - Scopi
                            - Diffondere i LSA a tutti i router della rete
                        - Funzionamento
                            - Si utilizzano i link state update (Type = 4) a fronte di un cambiamento nello stato di un collegamento, di una link state request, periodicamente (ogni 30 minuti)
                            - Si continua ad inviare lo stesso update finché non viene confermata la sua ricezione dai nodi adiacenti tramite il pacchetto link state acknowledgment (Type = 5)
            - Sinconizzazione e aggiornamento
                - Hello: i router scoprono l’esistenza reciproca
                - Exchange: si sceglie il master e lo slave, poi si confrontano i database
                - Update: si inviano richieste di aggiornamento ai router adiacenti per aggiornare il database (flooding)

                ![alt text](./res/ospffunc.png "Funzionamento dell'OSPF in synch/update")

    - Exterior Gateway Protocols (EGP)
        - Non guidati dall'ottimizzazione ma dall'economia/burocrazia
        - Tipi
            - Exterior Gateway Protocol (EGP, più o meno come distance vector)
                - Funzionalità
                    - Neighbor acquisition: verificare se esiste un accordo per diventare vicini
                    - Neighbor reachability: monitorare le connessioni con i vicini
                    - Network reachability: scambiare informazioni sulle reti raggiungibili da ciascun vicino
                - Limiti
                    - Pensata per rete ad albero e non a maglia (i cicli creano instabilità e lentezza di convergenza del protocollo)
                    - Non si adatta velocemente alle modifiche della topologia
                    - Non è sicura
            - Border Gateway Protocol (BGP)
                - Pro: sicurezza, affidabilità, supporto CIDR
                - Tipi di sessioni
                    - Esterne (eBGP) instaurate tra router BGP appartenenti ad AS diversi
                    - Interne (iBGP) instaurate tra router BGP appartenenti allo stesso AS
                - Path vector (evoluzione distance vector)
                    - Nel vettore dei percorsi si elencano tutti gli AS da attraversare per raggiungere una destinazione
                    - Risolve il problema dei cicli: quando un router di bordo di un AS riceve un path vector controlla se il suo AS è già elencato al suo interno, se lo è significa che esiste la possibilità di un loop e quel path vector non viene considerato, altrimenti il path vector viene aggiornato e comunicato ai vicini
                    - Politiche di routing (economia)
                        - Export policies: si comunicano ai vicini solo i path vector relativi alle destinazioni verso le quali si vuole permettere il transito 
                        - Import policies: dal path vector è possibile risalire agli AS da attraversare per raggiungere una destinazione; se nel path vector ricevuto da un vicino sono presenti uno o più AS incompatibili con le politiche di routing stabilite, esso viene ignorato
                    - Attributi
                        - Classi
                            - Well-known: riconoscibile da tutte le implementazioni BGP, deve essere inoltrato assieme al path vector
                                - Mandatory: deve essere presente nel path vector
                                - Discretionary: può anche non essere indicato
                            - Optional: può non essere riconosciuto da alcuni router
                                - Transitive: deve essere inoltrato anche se non riconosciuto
                                - Non-transitive: deve essere ignorato se non riconosciuto
                            - Partial: optional-transitive, indica se un determinato path vector è stato riconosciuto o meno da tutti i router attraversati
                        - Alcuni tipi
                            - Origin (1): well-known mandatory
                                - 0 = IGP: l’informazione è stata ottenuta direttamente dal protocollo di routing operante all’interno dell’AS in cui si trova la destinazione e per cui la si ritiene veritiera
                                - 1 = EGP: l’informazione è stata appresa dal protocollo EGP, che non funziona se vi sono cicli quindi un percorso caratterizzato da questo valore è peggiore di uno di tipo IGP
                                - 2 = incomplete: serve ad indicare che il percorso è stato determinato in altro modo oppure è utilizzato per marcare un percorso di AS che è stato troncato perché la destinazione è al momento non raggiungibile
                            - AS path (2): well-known mandatory
                                - Consiste nell’elenco degli AS da attraversare lungo il percorso verso la destinazione
                            - Next hop (3): well-known mandatory
                                - Indica l’indirizzo IP del router di bordo dell’AS che deve essere usato come next hop verso la destinazione specificata
                - Formato messaggi (generico)
                    - Marker: campo per possibile schema di autenticazione
                    - Length: numero di byte del messaggio BGP, header incluso
                    - Type
                        - Open: primo messaggio trasmesso quando viene attivata una connessione verso un router BGP vicino
                            - Informazioni di identificazione dell’AS di chi trasmette
                            - Durata del timeout per considerare un vicino non più attivo
                            - Dati di autenticazione
                        - Notification: messaggio di notifica di errori e/o di chiusura della connessione
                        - Update: contiene il path vector e i relativi attributi
                        - Keepalive: usato per comunicare ad un router BGP vicino che il trasmettitore è comunque attivo, anche se silente

### Parte 8

- Tipi di reti
    - Palazzo: LAN
    - Città: MAN
    - Stato, continente, pianeta: WAN
- LAN: infrastruttura di telecomunicazioni che consente ad apparati indipendenti(stazioni) di comunicare in un’area limitata attraverso un canale fisico condiviso ad elevato bit rate con bassi tassi di errore
    - Mezzo trasmissivo
        - Rame
        - Fibra
        - Radio
    - Topologie
        - Stella
        - Maglia completa/Maglia standard
        - Architettura gerarchica
        - Punto-multipunto (mezzo di trasmissione condiviso)
            - Tipi
                - Bus bidirezionale

                    ![alt text](./res/busbi.png "Bus bidirezionale")

                - Bus unidirezionale

                    ![alt text](./res/busuni.png "Bus unidirezionale")

                - Doppio bus (dual bus)

                    ![alt text](./res/doppiobus.png "Doppio bus")

                - Anello

                    ![alt text](./res/anello.png "Anello")

            - Problemi/caratteristiche
                - Broadcast: comunicazione nativa da uno a tutti sul mezzo condiviso
                    - Per evitare che tutti i calcolatori leggano i dati di tutte le comunicazioni si deve introdurre un meccanismo di indirizzamento a livello di protocollo LAN
                    - Soluzione: MAC
                        - 48 bit (6 byte)
                        - Scritti nella scheda di rete
                        - Univoci a livello mondiale
                        - Struttura
                            - Primi 3 byte individuano il costruttore
                            - Secondi 3 numerano progressivamente le schede
                        - Utilità
                            - Singolo destinatario (unicast)
                            - Gruppo di destinatari (multicast): il primo bit deve essere a 1
                            - Broadcast: ff-ff-ff-ff-ff-ff
                - Collisione: su di un mezzo condiviso esiste la possibilità che più utenti inviino informazioni contemporaneamente con conseguente interferenza o distruzione dell’informazione
                    - Soluzione: accesso multiplo a divisione di tempo
                        - Controllo centralizzato
                        - Controllo distribuito
                            - Assegnazione statica: per ogni connessione si usa un canale assegnato a priori in modo deterministico
                            - Assegnazione dinamica: la stazione impegna il mezzo solo quando ne ha bisogno
                                - Controllati o collision free: non ammettono collisioni
                                    - CAP (Channel Access Procedure): insieme delle procedure che la stazione effettua per realizzare l'accesso al canale
                                - A contesa: ammettono collisioni e cercano di porvi rimedio
                                    - CRA (Collision Resolution Algorithm): insieme delle procedure che la stazione effettua per rivelare ed eventualmente recuperare situazioni di collisione
- IEEE 802
    - Molte delle soluzioni adottate nei punti precedenti risultano obsolete, inefficienti e scomode; soluzione = IEEE 802
    - Scelta fondamentale: divisione degli strati 1 e 2 in due sottostrati
        - Strato 2
            - LLC (Logical link control): indipendente dal mezzo fisico, dalla topologia e dal protocollo di accesso
            - MAC (Medium access control)
        
        ![alt text](./res/IEEE802.png "Modello IEEE 802")

- Rete ethernet
    - Carrier Sensing Multiple Access with Collision Detection (CSMA/CD)
        - Limita molto ma non elimina la possibilità che due stazioni parlino in contemporanea
        - È quindi possibile la collisione fra due o più flussi informativi risultante in perdita di trame
        - Permette un’utilizzazione molto efficiente della banda disponibile sul mezzo trasmissivo
        - Non è in grado di garantire in modo certo i tempi  di consegna delle trame (ritardo di accesso)
    - Slot time: tempo per definire la fine della comunicazione (solitamente 30 secondi)
    - Collision domain: insieme delle stazioni connesse alla medesima rete Ethernet che possono collidere in trasmissione
        - E' conseguenza delle tecnologie adottate per lo strato fisico
            - Per garantire il corretto funzionamento del CSMA/CD si devono imporre vincoli alla dimensione massima della LAN in funzione della dimensione delle trame e della velocità di trasmissione
    - Tipologie
        - 802.3
            - 10base5
                - 10: velocità 10 Mbit/s
                - base: trasmissione in banda base
                - 5: segmenti fino a 500 metri (con max 100 stazioni)
                - Problema: si utilizza un cavo rigido, non si presta al cablaggio; occorre quindi distribuire capillarmente prese a muro per raggiungere tutte le stazioni
                - Si BUCA il filo per connettersi

                    ![alt text](./res/10b5.png "Connessione di un host a rete 10base5")

            - 10base2 (thin wire ethernet)
                - 2: segmenti fino a 180 metri con max 30 stazioni
                - Ci sono dei connettori apposta ma sono scomodi

                    ![alt text](./res/10b2.png "Connessione di un host a rete 10base2")

            - 10baseT
                - UTP (Unshielded Twisted Pairs)
                - Con cavo di categoria 3 si arriva a 100m
                - RJ45
                - "Giorni nostri"

                    ![alt text](./res/10bt.png "Connessione di un host a rete 10baseT")

            - 10baseF
                - Cavo in fibra ottica
                - Fino a 2000m di distanza
        - 802.3 u (Fast ethernet)
            - 100baseT4
                - Ogni stazione è collegata con 4 UTP di categoria 3
                - Lunghezza fino a 100m
                - Diametro massimo di un collision domain: 250 m
                - Sfrutta canali paralleli: su ognuna delle UTP la velocità e 33.333.. Mbit/s
            - 100baseTX
                - 2 coppie UTP di categoria 5
                - Lunghezza fino a 100m
                - Velocità netta 100 Mbit/s full duplex
            - 100baseFX
                - Cavo in fibra ottica
                - Fino a 2000 m
        - 802.3 z (Gigabit Ethernet)
            - 1000baseSX e 1000baseLX
            - 1000baseCX
                - 2 coppie intrecciate e schermate STP (nel cavetto ogni coppia è schermata e tutto l’insieme delle coppie è di nuovo schermato)
                - Costoso
            - 1000baseT
                - 4 coppie UTP di categoria 5
                - Velocità netta 1Gbit/s half duplex
        - 802.3 ae (Multigigabit Ethernet)
            - 10 Gigabit 
            - Applicabile solo su fibra ottica
            - Distanze fino a diversi Km
            - Vengono usate per il backbone
- Formato del frame (Ethernet vs IEEE 802.3)

    ![alt text](./res/frameIEEEeth.png "Formato del frame (Ethernet vs IEEE 802.3)")

    - Preamble: 7 byte tutti uguali a `10101010`; producono, a 10 Mbps, un'onda quadra che consente al ricevitore di sincronizzare il suo clock con quello del trasmettitore
    - SFD: `10101011` ha la funzione di flag di inizio frame
    - Lunghezza/Tipo    
        - IEEE 802.3: quanti byte ci sono nel campo dati (da 0 a 1500)
            - Tipo di payload è dato da LLC
            - I primo 4 bit sono sempre `0`
        - Ethernet: tipo di payload contenuto nel campo dati (uno dei primi 4 bit è ≠ `0`)
    - Dati: contiene il payload del livello superiore
    - Pad: se il frame (esclusi preambolo e delimiter) è più corto di 64 byte, con questo campo lo si porta alla lunghezza di 64 byte
    - Frame Checking sequence
    - Indirizzi (funzionamento del MAC)
- Reti moderne
    - Cablaggio
        - Organizzazione gerarchica

            ![alt text](./res/newcab.png "Cablaggio organizzato gerarchicamente")

        - Componenti
            - Prese a muro: punti di accesso alla LAN
            - Cablaggio orizzontale o di piano: cavi che collegano le prese a muro con l’armadio di rete realizzando una topologia a stella
            - Armadio di rete: punto di arrivo del cablaggio orizzontale e contenitore degli apparati attivi della LAN 
                - Patch panel
                    - Sul reto un insieme di connettori per cavi UTP
                        - Il cablaggio orizzontale entra nell’armadio di rete e i vari cavi sono collegati al retro dei patch panel
                    - Sul fronte un insieme di prese standard
                        - Queste saranno connesse agli apparati attivi mediante cavi dett patch cord
            - Cablaggio verticale: interconnette più armadi di rete
    - Wireless (802.11)
        - Strato fisico
            - Banda: ISM, Industrial Scientific Medical a 2.4 GHz o 5GHz
            - Regole di utilizzo
                - Obbligo di richiesta di autorizzazione al Ministero per offrire servizi Wi-Fi nella banda ISM su suolo pubblico
                - Obbligo di identificazione degli utenti di tali servizi
                - Nessun obbligo su suolo privato
        - Architettura
            - Componenti

                ![alt text](./res/arcwifi.png "Componenti di architetture wireless")

                ![alt text](./res/ess.png "Extended service set")

                ![alt text](./res/wds.png "Wireless distribution system")

            - Modalità
                - Infrastructure BSS: stazioni comunicano attraverso l’AP

                    ![alt text](./res/infbss.png "Modalità infrastruttura")

                    - Access point
                        - Configurato come bridge tra WLAN e LAN così l’intero ESS è visto come un’unica LAN
                        - Decide chi può trasmettere in modo da non far interferire i vari host fra loro
                            - In downlink non ci possono essere collisioni in quanto solo l'AP trasmette
                            - In uplink invece ci sono, per questo l'AP implementa metodi complessi per la gestione dei "turni" di trasmissione

                - Independent BSS: stazioni comunicano in modalità peer-to-peer e solo se si vedono direttamente

                    ![alt text](./res/modhoc.png "Modalità Ad-Hoc")

    - Interconnessione di più LAN mediante apparati
        - Repeater: permette l’estensione del mezzo di trasmissione
            - Strato OSI livello 1
            - Amplifica il segnale
            - Rigenera i bit entranti e li sincronizza
        - Bridge: interconnette lan di tipo diverso mediante conversioni
            - Strato OSI livello 2
            - Learning e Filtering
                - Impara quali stazioni sono connesse ad una porta analizzando il “source address” delle trame MAC
                - Invia la trama solo sulla porta di uscita dove si trova il destinatario analizzando il destination address della trama MAC
                - Separa il traffico dei diversi domini di collisione

            ![alt text](./res/bridge.png "Esempio di funzionamento di un bridge")
        
        - Switch: bridge ad alta densità di porte, in grado di trasferire contemporaneamente trame da più porte di ingresso a più porte di uscita
            - Strato OSI livello 2
            - Hub vs Switch
                - Hub
                    - Bus collassato = mezzo condiviso, trasmissione broadcast delle trame
                    - Capacità aggregata = capacità della singola porta
                - Switch
                    - Sistema di commutazione = ri-trasmissione selettiva delle trame
                    - Capacità aggregata superiore a quella della singola porta

                ![alt text](./res/hubvssw.png "Hub vs switch")
            
            - Interconnessione di LAN tramite switch

                ![alt text](./res/inlansw.png "Interconnessione di LAN tramite switch")
        
        - Interconnessione di LAN tramite router

            ![alt text](./res/intlanrou.png "Interconnessione di LAN tramite router")

### Parte 9

- Virtualizzazione: il sistema viene eseguito come elemento software logicamente indipendente dall’harware utilizzato
    - Virtualizzazione di rete
        - Introduzione
            - Perchè?
                - L’infrastruttura di rete non è facilmente modificabile
                - Le esigenze di servizio dell’utenza presentano una complessità sempre crescente
            - Obiettivo
                - Realizzare topologie o funzionalità sull’infrastruttura esistente diverse da quelle native (reti “overlay”, sovrapposte logicamente all’infrastruttura fisica per realizzare funzionalità diverse da quelle normalmente fornite dalla stessa)
        - Tipi
            - Virtual Local Area Network (VLAN)
                - Ogni VLAN rappresenta un diverso dominio broadcast
                - Direct forwarding fra Host della stessa VLAN
                - Indirect forwarding tramite gateway fra host di VLAN diverse

                ![alt text](./res/vlsn.png "Esempio di VLAN")

                - Classificazione
                    - Statiche
                        - Ogni porta dello switch è associata ad una VLAN
                        - Un host appartiene alla VLAN corrispondente alla porta a cui è connesso
                    - Dinamiche
                        - L'appartenenza alle VLAN è stabilita in base all’indirizzo dell’host (MAC o IP)
                - IEEE 802.1Q: protocollo che permette l’utilizzo delle stesse VLAN su diversi switch interconnessi tra loro
                    - Occorre specificare a quale VLAN appartiene una trama inviata ad un altro switch mediante l'etichetta "tag" del pacchetto nell'intestazione ethernet 
                - Inter-VLAN Routing
                    - Unica interfaccia fisica compatibile con il tagging 802.1Q
                    - N interfacce virtuali sulla stessa interfaccia fisica
                    - Ogni sub-interfaccia utilizza il VLAN ID corrispondente alla sua VLAN

                    ![alt text](./res/intervlan.png "Inter-VLAN Routing")

                - Porte dello switch
                    - Access mode: porta associata ad una sola VLAN
                    - Trunk mode: porta associata a VLAN multiple (802.1Q)

            - Virtual Extensible Local Area Network (VXLAN): come VLAN ma altamente scalabile?
            - Virtual Private Network (VPN): flusso punto-punto di pacchetti autenticati (con contenuto informativo criptato) incapsulati in pacchetti tradizionali spediti su rete pubbliche
                - Protocolli di tunnelling
                    - Livello 2: PPTP, L2TP
                    - Livello 3: IPsec
                        - Transport mode vs Tunnel mode

                            ![alt text](./res/tunvstran.png "Tunnel mode vs Transport mode")

                            - Problema: i messaggi che mando attraverso una VPN dovranno essere spezzati in quanto aggiungo del contenuto informativo al pacchetto (TCP in questo caso) di base, il quale risulta già "paddato" 

                - Prevenzione di

                    ![alt text](./res/risks.png "Rischi della comunicazione remota")

                - Tipi
                    - Roadwarrior
                        
                        ![alt text](./res/roadwar.png "Esempio di VPN di tipo Roadwarrior")

                        - Inefficiente se ho molti host co-localizzati

                    - Net-to-Net

                        ![alt text](./res/net2net.png "Esempio di VPN di tipo Net-to-Net")


